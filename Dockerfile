FROM node:dubnium-alpine

# Create app directory
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

# Install app dependencies
# COPY package.json yarn.lock ./
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

# ENV NPM_CONFIG_LOGLEVEL warn

# Install all dependencies of the current project.
# COPY package.json package.json

#install the application
# RUN yarn install

# Copy all local files into the image.
COPY . ./

ENV NPM_CONFIG_LOGLEVEL warn

# Install and configure `serve`.
# RUN yarn build
RUN yarn global add serve


EXPOSE 3000
CMD serve -l 3000 -s build
