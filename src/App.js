import React from "react";
import "./App.css";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "apollo-boost";
import { GRAPHQL_API_ENDPOINT } from "./config";
// import LoadingScreen from './components/LoadingScreen';
import SessionRouter from "./routes/SessionRouter";
import AppRouter from "./routes/AppRouter";

import User from "./components/User";

export const client = new ApolloClient({
  uri: GRAPHQL_API_ENDPOINT,
  credentials: "include",
  headers: {
    AppName: "Admin",
  },
});

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <div>
          <User>
            {(data) => {
              // console.log("current user query data output", data);
              //console.log(data.currentUser)
              // if(data.currentUser){
              if (data && data.currentUser) {
                return <AppRouter />;
              } else {
                return <SessionRouter />;
              }
            }}
          </User>
        </div>
      </ApolloProvider>
    </div>
  );
}

export default App;
