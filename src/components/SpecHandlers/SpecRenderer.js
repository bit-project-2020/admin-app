import React from "react";
import styled from "styled-components";
import { Card } from "antd";
import { Divider } from "antd";
import {
  EditOutlined,
  PlusOutlined,
  PlayCircleOutlined,
  PauseCircleOutlined,
} from "@ant-design/icons";

const SpecCardWrapper = styled.div`
  h1.param-label {
    text-align: right;
    display: inline-block;
  }

  .card-set {
    display: flex;
    flex-wrap: wrap;
  }

  .active {
    color: green;
  }

  .inactive {
    color: red;
  }
`;

const SpecRenderer = ({
  specs,
  plantParameters,
  setModalOpen,
  setItemToChange,
  sectionTitle,
  size,
  updateFunction,
}) => {
  // create a populated object "plantParametersWithSpec"
  const plantParameterWithSpec = [];

  plantParameters.forEach((plantParam) => {
    const matchingSpec = specs.find(
      (spec) => spec.plant_parameter === plantParam.id
    );

    if (matchingSpec) {
      plantParameterWithSpec.push({ ...plantParam, spec: matchingSpec });
    } else {
      plantParameterWithSpec.push({ ...plantParam });
    }
  });

  if (plantParameters.length === 0) {
    return <div></div>;
  }

  return (
    <SpecCardWrapper>
      <Divider orientation="left">
        <h1> specifications for the size "{sectionTitle}" </h1>
      </Divider>
      <div className="card-set">
        {plantParameterWithSpec.map((param) => {
          const cardActions = [];
          // to create a new spec
          if (param.spec === undefined) {
            cardActions.push(
              <PlusOutlined
                key="setting"
                onClick={() => {
                  setModalOpen(true);
                  setItemToChange({
                    plant_parameter: param.id,
                    unit: param.unit,
                    parameter_type: param.parameter_type,
                    parameter: param.parameter,
                    sectionTitle: sectionTitle,
                    size,
                  });
                }}
              />
            );
          } else {
            // you can update
            cardActions.push(
              <EditOutlined
                key="setting"
                onClick={() => {
                  console.log("do nothing");
                  setModalOpen(true);
                  setItemToChange({
                    spec: param.spec,
                    isUpdate: true,
                    unit: param.unit,
                    plant_parameter: param.id,
                    parameter_type: param.parameter_type,
                    parameter: param.parameter,
                    sectionTitle: sectionTitle,
                    size: size,
                  });
                }}
              />
            );
            // deactivate/reactivate spec
            if (param.spec.is_active === true) {
              cardActions.push(
                <PauseCircleOutlined
                  key="setting"
                  onClick={() => {
                    updateFunction({
                      variables: {
                        where: {
                          id: param.spec.id,
                        },
                        data: {
                          is_active: false,
                        },
                      },
                    });
                  }}
                />
              );
            } else {
              cardActions.push(
                <PlayCircleOutlined
                  key="setting"
                  onClick={() => {
                    updateFunction({
                      variables: {
                        where: {
                          id: param.spec.id,
                        },
                        data: {
                          is_active: true,
                        },
                      },
                    });
                  }}
                />
              );
            }
          }

          return (
            <Card
              key={param.id}
              style={{ width: 200, margin: "10px 10px", textAlign: "center" }}
              actions={cardActions}
              // parameter_type={param.parameter_type}
            >
              <h1 className="param-label"> {param.parameter}</h1> <br />
              {param.spec !== undefined && param.parameter_type === "NUMBER" && (
                <>
                  <span>
                    min: {param.spec && param.spec.value.split(",")[0]}
                  </span>
                  &nbsp; &nbsp;
                  <span>
                    max: {param.spec && param.spec.value.split(",")[1]}
                  </span>
                </>
              )}
              {param.spec !== undefined && param.parameter_type === "BOOLEAN" && (
                <>
                  <span>
                    expected value: &nbsp;
                    {param.spec && param.spec.value === "true"
                      ? "ok"
                      : "not-ok"}
                  </span>
                </>
              )}
              {param.spec !== undefined &&
                param.parameter_type === "NUMBER_EXACT" && (
                  <>
                    <span>
                      expected value:
                      {param.spec && param.spec.value}
                    </span>
                  </>
                )}
              {param.spec === undefined && (
                <span>No specificatoin defined</span>
              )}
              {param.spec && param.spec.is_active === true ? (
                <div className="active">Active</div>
              ) : (
                <span>
                  {param.spec ? (
                    <div className="inactive">Inactive</div>
                  ) : (
                    <div></div>
                  )}
                </span>
              )}
            </Card>
          );
        })}
      </div>
    </SpecCardWrapper>
  );
};

export default SpecRenderer;
