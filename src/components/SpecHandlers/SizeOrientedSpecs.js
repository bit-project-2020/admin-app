import React from "react";
import styled from "styled-components";
import { Card } from "antd";
import { Divider } from "antd";
import {
  EditOutlined,
  PlusOutlined,
  PlayCircleOutlined,
  PauseCircleOutlined,
} from "@ant-design/icons";

const SpecCardWrapper = styled.div`
  h1.param-label {
    text-align: right;
    display: inline-block;
  }
`;

const SizeOrientedSpecs = ({
  specs,
  plantParameters,
  setModalOpen,
  setItemToChange,
  sectionTitle,
}) => {
  // match every plant parameter with it's specification
  plantParameters.forEach((plantParam) => {
    const matchingSpec = specs.find(
      (spec) => spec.plant_parameter === plantParam.id
    );
    plantParam.spec = matchingSpec;
  });

  // console.log(445566, plantParameters);
  console.log(445566, plantParameters);

  return (
    <SpecCardWrapper>
      <Divider orientation="left">
        <h1> specifications for the size "{sectionTitle}" </h1>
      </Divider>

      {/* use below component if you wanna seperate product and packaging specs */}
      {/* <div id="product_specs"></div>
      <div id="packaging-specs"></div> */}

      {plantParameters.map((param) => {
        const cardActions = [];
        // to create a new spec
        if (param.spec === undefined) {
          cardActions.push(
            <PlusOutlined
              key="setting"
              onClick={() => {
                setModalOpen(true);
                setItemToChange({
                  //...param,
                  plant_parameter: param.id,
                  parameter_type: param.parameter_type,
                  parameter: param.parameter,
                  size: sectionTitle,
                });
              }}
            />
          );
        } else {
          // you can update
          cardActions.push(
            <EditOutlined
              key="setting"
              onClick={() => {
                console.log("do nothing");
                setModalOpen(true);
                setItemToChange({
                  //...param,
                  spec: param.spec,
                  isUpdate: true,
                  plant_parameter: param.id,
                  parameter_type: param.parameter_type,
                  parameter: param.parameter,
                  size: sectionTitle,
                });
                // setTheItemToUpdate(size);
                // setInitialView("update");
              }}
            />
          );
          // deactivate/reactivate spec
          if (param.is_active === true) {
            cardActions.push(
              <PauseCircleOutlined
                key="setting"
                onClick={() => {
                  console.log("do nothing");
                  // setTheItemToUpdate(size);
                  // setInitialView("update");
                }}
              />
            );
          } else {
            cardActions.push(
              <PlayCircleOutlined
                key="setting"
                onClick={() => {
                  console.log("do nothing");
                  // setTheItemToUpdate(size);
                  // setInitialView("update");
                }}
              />
            );
          }
        }

        return (
          <Card
            key={param.id}
            style={{ width: 200, margin: "10px 10px", textAlign: "center" }}
            actions={cardActions}
            // parameter_type={param.parameter_type}
          >
            <h1 className="param-label"> {param.parameter}</h1> <br />
            {param.spec !== undefined && param.parameter_type === "NUMBER" && (
              <>
                <span>min: {param.spec && param.spec.value.split(",")[0]}</span>
                <span>max: {param.spec && param.spec.value.split(",")[1]}</span>
              </>
            )}
            {param.spec !== undefined && param.parameter_type === "BOOLEAN" && (
              <>
                <span>
                  expected value:
                  {param.spec && param.spec.value === "true" ? "ok" : "not-ok"}
                </span>
              </>
            )}
            {param.spec === undefined && <span>No specificatoin defined</span>}
          </Card>
        );
      })}
    </SpecCardWrapper>
  );
};

export default SizeOrientedSpecs;
