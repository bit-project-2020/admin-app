import React from "react";
import NCRForm from "../forms/ncrForm";

const NCRData = ({ data, refetch, refetchParams }) => {
  return (
    <div>
      <NCRForm
        isUpdate={true}
        initialValues={data}
        refetch={refetch}
        refetchParams={refetchParams}
      />
    </div>
  );
};

export default NCRData;
