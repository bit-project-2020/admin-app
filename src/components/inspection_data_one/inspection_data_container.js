import React from "react";

import { Table, Space, Button, Image, Modal } from "antd";
import styled from "styled-components";
import { useMutation } from "@apollo/react-hooks";
import GetUserData from "../../components/GetUserData";
import { withRouter } from "react-router-dom";
import { CREATE_NCR_MUTATION } from "../../mutations/CreateNCR";
import { UPDATE_INSPECTION_RECORD_MUTATION } from "../../mutations/UpdateInspectionRecord";
import User from "../../components/User";

import { INSPECTION_RECORD_ONE_QUERY } from "../../queries/inspectionRecordOne";

import showNotifications from "../../lib/showNotifications";

const { confirm } = Modal;

const InspectionDataOneWrapper = styled.div`
  .section-title {
    color: Red;
    font-size: 1.2rem;
  }

  div#ncr-create-button {
    float: right;
  }

  div.make-bold,
  span.field-title {
    font-weight: bold;
    display: inline-block;
    min-width: 120px;
    text-align: right;
  }
  button {
    margin-right: 5px;
  }
`;

const columns = () => {
  return [
    {
      title: "Parameter",
      dataIndex: "parameter",
      key: "parameter",
    },
    {
      title: "Value",
      key: "parameter",
      render: (record) => {
        //console.log(600600, record);
        if (record.type === "BOOLEAN") {
          return <>{record.boolean_value ? "ok" : "not_ok"}</>;
        }
        if (record.type === "NUMBER" || record.type === "NUMBER_EXACT") {
          return <>{record.numeric_value}</>;
        }
        if (record.type === "DATE") {
          const date = new Date(parseInt(record.text_value));
          console.log(record.text_value);
          return <>{date.toLocaleDateString()}</>;
        }
      },
    },
    {
      title: "Latest Specification",
      // dataIndex: "specification",
      key: "specification",
      render: (record) => {
        // console.log(600600, record);
        if (record.specification) {
          if (record.type === "BOOLEAN") {
            return <>{record.specification ? "ok" : "not_ok"}</>;
          }
          if (record.type === "NUMBER") {
            return (
              <>
                min: {record.specification.split(",")[0]} , max:{" "}
                {record.specification.split(",")[1]}
              </>
            );
          }
          if (record.type === "NUMBER_EXACT") {
            return <>{record.specification}</>;
          }
          if (record.type === "DATE") {
            return <>N/A</>;
          }
        } else {
          return <>N/A</>;
        }
      },
    },
  ];
};

const deviationTablecolumns = (extra_fields) => {
  return [
    {
      title: "Parameter",
      dataIndex: "parameter",
      key: "parameter",
    },
    {
      title: "Value",
      key: "parameter",
      render: (record) => {
        const found_param = extra_fields.find(
          (field) => field.parameter === record.parameter
        );

        //console.log(600600, record);
        if (found_param.type === "BOOLEAN") {
          return <>{record.value === "true" ? "Ok" : "Not Ok"}</>;
        }
        if (found_param.type === "NUMBER" || record.type === "NUMBER_EXACT") {
          return <>{record.value}</>;
        }
        if (found_param.type === "DATE") {
          const date = new Date(parseInt(record.text_value));
          console.log(record.value);
          return <>{date.toLocaleDateString()}</>;
        }
      },
    },
    {
      title: "Specification",
      key: "specification",
      render: (record) => {
        if (record.specification) {
          const found_param = extra_fields.find(
            (field) => field.parameter === record.parameter
          );

          if (found_param.type === "BOOLEAN") {
            return <>{record.specification ? "Ok" : "Not Ok"}</>;
          }
          if (found_param.type === "NUMBER") {
            return (
              <>
                min: {record.specification.split(",")[0]} , max:{" "}
                {record.specification.split(",")[1]}
              </>
            );
          }
          if (found_param.type === "NUMBER_EXACT") {
            return <>{record.specification}</>;
          }
          if (found_param.type === "DATE") {
            return <>N/A</>;
          }
        } else {
          return <>N/A</>;
        }
      },
    },
  ];
};

const InspectionDataContainer = ({
  record_type,
  plant,
  record_id,
  data,
  refetchTheRecord,
  history,
  pathname,
}) => {
  console.log(300400, data);
  const [
    createNCR,
    // { data, loading, error}
  ] = useMutation(CREATE_NCR_MUTATION, {
    onCompleted: () => {
      // refetch the current record. so that the create ncr button will go disabled since there is already an ncr
      showNotifications({ message: "NCR successfully created!" });
      refetchTheRecord({
        record_type,
        plant,
        record_id,
      });

      history.push(
        `/${pathname.split("/")[1]}/${plant}/${record_id}?focusNCR=true`
      ); // redirect with the ncrFocus flag
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: INSPECTION_RECORD_ONE_QUERY,
        variables: {
          record_type,
          plant,
          record_id,
        },
      },
    ],
  });

  const [
    updateInspectionRecord,
    // { data, loading, error}
  ] = useMutation(UPDATE_INSPECTION_RECORD_MUTATION, {
    onCompleted: () => {
      // refetch the current record. so that the create ncr button will go disabled since there is already an ncr
      showNotifications({ message: "Record Approved!" });
      refetchTheRecord({
        record_type,
        plant,
        record_id,
      });
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: INSPECTION_RECORD_ONE_QUERY,
        variables: {
          record_type,
          plant,
          record_id,
        },
      },
    ],
  });

  if (data) {
    // my job is to visualize them accordingly now.
    const { inspectionDataRecord: record } = data;
    console.log(655555, record);

    // seperate image_fields and remove them from the table

    const toTable = [];
    const notToTable = [];

    record.extra_fields.forEach((field) => {
      if (["IMAGE"].includes(field.type)) {
        notToTable.push(field);
      } else {
        toTable.push(field);
      }
    });

    const newDate = new Date(Date.UTC(parseInt(record.date)));

    // allow qam or qae to create the ncr

    return (
      <InspectionDataOneWrapper>
        <Space direction={"horizontal"} id="ncr-create-button">
          <User>
            {(data) => {
              return (
                <span>
                  <Button
                    disabled={
                      [
                        "PRODUCTION_MANAGER",
                        "PRODUCTION_OFFICER",
                        "QA_OFFICER",
                        "SYSTEM_ADMIN",
                      ].includes(data.currentUser.role) || record.approved_by
                    }
                    type="danger"
                    onClick={() => {
                      confirm({
                        okText: "Approve",
                        okType: "danger",
                        cancelText: "Cancel",
                        title:
                          "You are going to approve the data record. this action is imposible to reverse",
                        content:
                          "Please make sure you have carefully inspect the deviated product",
                        onOk() {
                          // update NCR
                          updateInspectionRecord({
                            variables: {
                              where: {
                                plant: plant,
                                record_type: record_type,
                                recordId: record.id,
                              },
                              data: {
                                approved_by: data.currentUser.id,
                              },
                            },
                          });
                        },
                        onCancel() {},
                      });

                      //history.push("/inspectionData");
                    }}
                  >
                    Approve
                  </Button>

                  <Button
                    disabled={
                      record.ncr ||
                      !record.deviations.length > 0 ||
                      [
                        "PRODUCTION_MANAGER",
                        "PRODUCTION_OFFICER",
                        "QA_OFFICER",
                        "SYSTEM_ADMIN",
                      ].includes(data.currentUser.role)
                    }
                    type="danger"
                    onClick={() => {
                      confirm({
                        okText: "Create",
                        okType: "danger",
                        cancelText: "Cancel",
                        title:
                          "You are going to create a non confirmity report.",
                        content:
                          "Please make sure you have carefully inspect the deviated product",
                        onOk() {
                          createNCR({
                            variables: {
                              data: {
                                plant: plant,
                                product: record.product.id,
                                size: record.size.id,
                                record_id: record.id,
                                type: record_type,
                              },
                            },
                          });
                        },
                        onCancel() {},
                      });

                      //history.push("/inspectionData");
                    }}
                  >
                    Create NCR
                  </Button>
                </span>
              );
            }}
          </User>
        </Space>
        <br />
        <Space direction={"vertical"}>
          <div>
            <span className="field-title"> Record ID :</span>
            <span> {record.id}</span>
          </div>
          <div>
            <span className="field-title"> Product Name :</span>
            <span> {record.product.name}</span>
          </div>
          <div>
            <span className="field-title"> Size :</span>
            <span> {record.size.size_name}</span>
          </div>
          <div>
            <span className="field-title"> Created By :</span>
            <span>
              <GetUserData id={record.created_by} />
            </span>
          </div>
          <div>
            <span className="field-title"> Approved By :</span>
            <span>
              {record.approved_by ? (
                <GetUserData id={record.approved_by} />
              ) : (
                <span> Pending Approval </span>
              )}
            </span>
          </div>

          <div>
            <span className="field-title"> Date :</span>
            <span> {newDate.toLocaleDateString()}</span>
          </div>

          <div>
            <span className="field-title"> Time :</span>
            <span> {newDate.toLocaleTimeString()}</span>
          </div>

          <div>
            <span className="field-title"> Shift :</span>
            <span> {record.shift}</span>
          </div>
        </Space>

        <br />
        <br />
        {/* //time to render extra fields with specs */}
        <Table
          // loading={loading}
          bordered
          rowKey={(record) => record.parameter}
          columns={columns()}
          dataSource={toTable}
          pagination={false}
        />
        <br />
        <br />
        {/* render image fields */}
        {notToTable.map((item) => {
          if (item.type === "IMAGE") {
            return (
              <div key={item.parameter}>
                <div className="make-bold">
                  {item.parameter} &nbsp;:
                  <br /> <br />
                </div>
                <Image width={200} src={item.text_value} />
              </div>
            );
          } else {
            return <></>;
          }
        })}
        <br />
        <br />
        {/* time to render deviations */}
        <div className="section-title"> Reported Deviations </div>

        <Table
          // loading={loading}
          bordered
          rowKey={(record) => record.parameter}
          columns={deviationTablecolumns(record.extra_fields)}
          dataSource={record.deviations}
          pagination={false}
        />

        {/* {record.deviations.length > 0 &&
          record.deviations.map((deviation) => {
            console.log(4001000, deviation);
            return (
              <div key={deviation.parameter}>
                <div className="deviation-key make-bold">
                  {deviation.parameter}
                </div>
                <div className="deviation-key"> {deviation.value} </div>
                <div className="deviation-key"> {deviation.specification}</div>
              </div>
            );
          })
        } */}
      </InspectionDataOneWrapper>
    );
  }
};

export default withRouter(InspectionDataContainer);
