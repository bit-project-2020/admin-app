import React from "react";
import { Layout, Menu } from "antd";
import { Link, withRouter } from "react-router-dom";
import User from "../components/User";

import {
  HomeOutlined,
  ShopOutlined,
  FileTextOutlined,
  DatabaseOutlined,
  UsergroupAddOutlined,
  FileAddOutlined,
  SwitcherOutlined,
  StockOutlined,
  PieChartOutlined,
} from "@ant-design/icons";

import ChandimaSweetsLogo from "../assets/images/logo.svg";

const { SubMenu } = Menu;
const { Sider } = Layout;
// const { SubMenu } = Menu;

const pageNames = {
  "": ["0"],
  plants: ["1"],
  parameters: ["2"],
  "process-data": ["3"],
  "packaging-data": ["4"],
  users: ["5"],
  products: ["6"],
  logs: ["7"],
  backup: ["8"],
  ncr: ["9"],
};

const Sidebar = ({ collapsed, onCollapse, location }) => {
  const pathname = location.pathname.split("/")[1];

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo">
        <img
          id="halo-logo"
          src={ChandimaSweetsLogo}
          alt="chandima sweets logo"
        />
      </div>
      <User>
        {(data) => {
          const { role } = data.currentUser;

          return (
            <Menu
              theme="dark"
              defaultSelectedKeys={pageNames[pathname]}
              mode="inline"
            >
              <Menu.Item key="0">
                <Link to="/">
                  <HomeOutlined />
                  <span>Home</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="1">
                <Link to="/plants">
                  <ShopOutlined />
                  <span>Plants</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/parameters">
                  <FileTextOutlined />
                  <span>Parameters</span>
                </Link>
              </Menu.Item>

              <Menu.Item key="3">
                <Link to="/process-data">
                  <DatabaseOutlined />
                  <span>Process Data</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/packaging-data">
                  <DatabaseOutlined />
                  <span>Packaging Data</span>
                </Link>
              </Menu.Item>
              <Menu.Item
                key="5"
                disabled={!["SYSTEM_ADMIN"].includes(role) ? true : false}
              >
                <Link to="/users">
                  <UsergroupAddOutlined />
                  <span>Users</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="6">
                <Link to="/products">
                  <FileAddOutlined />
                  <span>Products</span>
                </Link>
              </Menu.Item>
              <Menu.Item
                key="7"
                disabled={
                  ["PRODUCTION_MANAGER", "PRODUCTION_OFFICER"].includes(role)
                    ? true
                    : false
                }
              >
                <Link to="/logs">
                  <FileAddOutlined />
                  <span>QAO Logs</span>
                </Link>
              </Menu.Item>
              <Menu.Item
                key="8"
                disabled={!["SYSTEM_ADMIN"].includes(role) ? true : false}
              >
                <Link to="/backup">
                  <FileAddOutlined />
                  <span>Backup</span>
                </Link>
              </Menu.Item>

              <Menu.Item key="9">
                <Link to="/ncr">
                  <FileAddOutlined />
                  <span>NCR</span>
                </Link>
              </Menu.Item>

              <SubMenu
                key="sub1"
                title={
                  <>
                    <SwitcherOutlined />
                    <span>Reports</span>
                  </>
                }
              >
                <Menu.Item key="10">
                  <Link to="/reports/defect-Severity">
                    <PieChartOutlined />
                    <span>Defect Severity</span>
                  </Link>
                </Menu.Item>
                {/* <Menu.Item key="11">
            <Link to="/reports/defects-vs-time">
              <span> defects over time </span>
            </Link>
          </Menu.Item> */}
                {/* <Menu.Item key="12">
            <Link to="/reports/user-wise-reports">
              <StockOutlined />
              <span> User wise defects </span>
            </Link>
          </Menu.Item> */}
                <Menu.Item key="13">
                  <Link to="/reports/ncr-count">
                    <StockOutlined />
                    <span> Weekly NCR summary </span>
                  </Link>
                </Menu.Item>
              </SubMenu>
            </Menu>
          );
        }}
      </User>
    </Sider>
  );
};

export default withRouter(Sidebar);
