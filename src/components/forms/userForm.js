import React from "react"; // , { useState }
import { Formik } from "formik";

import { Button, Form as AntForm, Select, Input, Checkbox, Upload } from "antd";

import ImgCrop from "antd-img-crop";
import uploadFile from "../../lib/uploadFile";

import styled from "styled-components";

import * as Yup from "yup";

const UserFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const UserFormSchema = Yup.object().shape({
  first_name: Yup.string()
    .min(1, "Too Short!")
    .max(25, "Too Long!")
    // .test("contain-spaces", "spaces are not allowed", (value) => {
    //   if (value !== undefined && value !== null) {
    //     if (typeof value === "string") {
    //       return !value.includes(" ");
    //     }
    //   } else {
    //     return true;
    //   }
    // })
    .required("First name is a required field"),
  last_name: Yup.string()
    .min(1, "Too Short!")
    .max(25, "Too Long!")
    .required("Last name is a required field"),
  role: Yup.string().required("Please select a role"),
  email: Yup.string().email("Please enter a valid email").required(),
  // is_active: Yup.boolean().required("a value is expected"),
});

const FormItem = AntForm.Item;

const { Option } = Select;

const UserForm = ({
  isUpdate,
  initialValues,
  updateFunction,
  createFunction,
}) => {
  if (!isUpdate) {
    initialValues = {
      first_name: "",
      last_name: "",
      role: "",
      email: "",
      is_active: true,
    };
  }

  return (
    <UserFormWrapper>
      <Formik
        validateOnBlur={true}
        validateOnChange={false}
        initialValues={{ ...initialValues }}
        validationSchema={UserFormSchema}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            const updatedValues = { ...values };

            setSubmitting(true);

            // console.log(
            //   77889955,
            //   values["profile_photo"],
            //   values["profile_photo"][0].status,
            //   values.profile_photo[0].originFileObj
            // );

            if (
              values["profile_photo"] &&
              values["profile_photo"].length !== 0 &&
              !values["profile_photo"][0].url
            ) {
              console.log(78989, values["profile_photo"]);
              updatedValues["profile_photo"] = await uploadFile({
                file: values.profile_photo[0].originFileObj,
                uploadPreset: "profile_photoes",
              });
            } else if (
              values["profile_photo"] &&
              values["profile_photo"].length === 0 // if added the image then delete before click on create
            ) {
              delete updatedValues.profile_photo;
            } else {
              // updatedValues["profile_photo"] = values.profile_photo[0].url;
            }

            if (isUpdate) {
              // delete conflicting fields
              delete updatedValues.id;
              delete updatedValues.__typename;

              console.log(98569856, updatedValues);
              if (
                values["profile_photo"] &&
                values["profile_photo"].length === 0
              ) {
                updatedValues.profile_photo = null;
              }

              updateFunction({
                variables: {
                  where: {
                    id: parseInt(initialValues.id),
                  },
                  data: {
                    ...updatedValues,
                  },
                },
              });
            } else {
              createFunction({
                variables: {
                  data: {
                    ...updatedValues,
                  },
                },
              });
              // run create function
            }

            setSubmitting(false);
          } catch (e) {
            console.log(e);
          }
        }}
      >
        {({
          handleSubmit,
          isSubmitting,
          handleBlur,
          handleChange,
          errors,
          values,
          touched,
          setFieldValue,
        }) => {
          return (
            <form onSubmit={handleSubmit}>
              <FormItem
                key="first_name"
                label="first_name"
                validateStatus={
                  touched["first_name"] && errors["first_name"]
                    ? "error"
                    : "validating"
                }
                help={touched["first_name"] && errors["first_name"]}
                required={true} // this will show that the fields are required
              >
                <Input
                  value={values.first_name}
                  onChange={handleChange}
                  name="first_name"
                  placeholder="First Name"
                  onBlur={handleBlur}
                  // required={true}
                />
              </FormItem>

              <FormItem
                key="last_name"
                label="last_name"
                validateStatus={
                  touched["last_name"] && errors["last_name"]
                    ? "error"
                    : "validating"
                }
                help={touched["last_name"] && errors["last_name"]}
                required={true}
              >
                <Input
                  value={values.last_name}
                  onChange={handleChange}
                  name="last_name"
                  placeholder="Last Name"
                  onBlur={handleBlur}
                  // required={true}
                />
              </FormItem>

              <FormItem
                label="Role"
                validateStatus={
                  touched["role"] && errors["role"] ? "error" : "validating"
                }
                help={touched["role"] && errors["role"]}
                required={true}
                placeholder="plant"
              >
                <Select
                  mode="default"
                  disabled={isSubmitting}
                  name="role"
                  value={values.role}
                  // showSearch
                  // defaultActiveFirstOption={false}
                  showArrow
                  onBlur={handleBlur}
                  placeholder="role"
                  filterOption={false}
                  // onSearch={searchField.search}
                  // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
                  onSelect={(value) => {
                    setFieldValue("role", value);
                  }}
                >
                  <Option value="QA_OFFICER" key="1">
                    QA_OFFICER
                  </Option>
                  <Option value="QA_EXECUTIVE" key="2">
                    QA_EXECUTIVE
                  </Option>
                  <Option value="QA_MANAGER" key="3">
                    QA_MANAGER
                  </Option>
                  <Option value="PRODUCTION_OFFICER" key="4">
                    PRODUCTION_OFFICER
                  </Option>
                  <Option value="PRODUCTION_MANAGER" key="5">
                    PRODUCTION_MANAGER
                  </Option>
                  <Option value="SYSTEM_ADMIN" key="6">
                    SYSTEM_ADMIN
                  </Option>
                </Select>
              </FormItem>

              {/* <FormItem
              key="emp_no"
              label="Employee Number"
              // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
              // help={errors[formItem.name]}
              // required={true}
            >
              <InputNumber
                value={values.emp_no}
                onChange={(value) => {
                  setFieldValue("emp_no", value);
                }}
                name="emp_no"
                placeholder="Emp. No."
                //required={true}
              />
            </FormItem> */}
              <FormItem
                key="email"
                label="Email Address"
                validateStatus={
                  touched["email"] && errors["email"] ? "error" : "validating"
                }
                help={touched["email"] && errors["email"]}
                required={true}
              >
                <Input
                  value={values.email}
                  onChange={handleChange}
                  name="email"
                  placeholder="Email address"
                  onBlur={handleBlur}
                  //required={true}
                />
              </FormItem>

              <FormItem key="is_active" label="is_active" required={true}>
                <Checkbox
                  defaultChecked={initialValues["is_active"]}
                  value={values.is_active}
                  onChange={handleChange}
                  name="is_active"
                  //required={true}
                />
              </FormItem>

              <FormItem
                key="Profile Picture"
                label="Profile Photo"
                // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                // help={errors[formItem.name]}
                required={false}
              >
                <ImgCrop>
                  <Upload
                    maxCount={1}
                    name="profile_photo"
                    accept="image/*"
                    listType="picture-card"
                    fileList={values["profile_photo"]}
                    onChange={({ fileList }) => {
                      console.log(54612, fileList.slice(-1));
                      setFieldValue("profile_photo", fileList.slice(-1));
                    }}

                    // onPreview={onPreview}
                  >
                    {"+ Upload"}
                  </Upload>
                </ImgCrop>
              </FormItem>

              <FormItem
                wrapperCol={{
                  xs: { span: 24, offset: 0 },
                  sm: { span: 6, offset: 3 },
                  xl: { span: 6, offset: 2 },
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                  //icon={<PlusOutlined />}
                >
                  {isUpdate ? "Update" : "Create"} User
                </Button>
              </FormItem>
            </form>
          );
        }}
      </Formik>
    </UserFormWrapper>
  );
};

export default UserForm;
