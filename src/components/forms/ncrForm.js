import React from "react"; // , { useState }
import { Formik } from "formik";
import styled from "styled-components";

import { USERS_QUERY } from "../../queries/users";

import { useQuery, useMutation } from "@apollo/react-hooks";
import { UPDATE_NCR_MUTATION } from "../../mutations/UpdateNCR";

import showNotifications from "../../lib/showNotifications";

import GetUserData from "../../components/GetUserData";

import User from "./../User";

import { Button, Form as AntForm, Input, Checkbox, Space, Select } from "antd";

const { TextArea } = Input;
const { Option } = Select;

const FormItem = AntForm.Item;

const NCRWrapper = styled.div`
  margin-top: 1.5rem;
  span.field-title {
    font-weight: bold;
    display: inline-block;
    min-width: 120px;
    text-align: right;
  }

  .ant-col.ant-form-item-label {
    min-width: 200px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

// const PlantFormSchema = Yup.object().shape({
//   plant_name: Yup.string()
//     .min(1, "Too Short!")
//     .max(15, "Too Long plant name!")
//     .test("contain-spaces", "spaces are not allowed", (value) => {
//       if (value !== undefined && value !== null) {
//         if (typeof value === "string") {
//           return !value.includes(" ");
//         }
//       } else {
//         return true;
//       }
//     })
//     .required("Plant name is a required field"),
//   is_active: Yup.boolean().required("a value is expected"),
// });

const NCRForm = ({
  isUpdate,
  initialValues,
  updateFunction,
  createFunction,
  refetch,
  refetchParams,
}) => {
  const { ncr } = initialValues;
  console.log(451232, initialValues);
  const newDate = new Date(Date.UTC(parseInt(initialValues.date)));

  const { loading, error, data } = useQuery(USERS_QUERY);

  const [
    updateNCR,
    // { data, loading, error}
  ] = useMutation(UPDATE_NCR_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "NCR successfully updated!" });
      refetch({
        ...refetchParams,
      });

      // history.push(
      //   `/${pathname.split("/")[1]}/${plant}/${record_id}?focusNCR=true`
      // );
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
  });

  if (loading) {
    return <h1> Loading....! </h1>;
  }

  if (error) {
    return <div>Error</div>;
  }

  if (data) {
    const { users } = data;

    return (
      <NCRWrapper>
        <Space direction={"vertical"}>
          <div>
            <span className="field-title"> Record ID: </span>
            <span>{initialValues.ncr.id}</span>
          </div>
          <div>
            <span className="field-title"> Created By: </span>
            <span>
              <GetUserData id={initialValues.created_by} />
            </span>
          </div>
          <div>
            <span className="field-title"> Plant: </span>
            <span>{initialValues.product.plant}</span>
          </div>
          <div>
            <span className="field-title"> Product: </span>
            <span>{initialValues.product.name}</span>
          </div>
          <div>
            <span className="field-title"> Created At: </span>
            <span> {newDate.toLocaleDateString()}</span>
          </div>
          <div>
            <span className="field-title"> Time: </span>
            <span> {newDate.toLocaleTimeString()}</span>
          </div>
        </Space>
        <br /> <br />
        <Formik
          validateOnBlur={true}
          validateOnChange={false}
          initialValues={{ ...ncr }}
          // validationSchema={PlantFormSchema}
          onSubmit={async (values, { setSubmitting }) => {
            try {
              const updatedValues = { ...values };
              setSubmitting(true);

              console.log(111111, ncr);
              console.log(222222, updatedValues);

              // delete conflicting fields

              const updateVariables = {};

              if (ncr.qa_representative !== updatedValues.qa_representative) {
                updateVariables.qa_representative =
                  updatedValues.qa_representative;
              }
              if (
                ncr.production_representative !==
                updatedValues.production_representative
              ) {
                updateVariables.production_representative =
                  updatedValues.production_representative;
              }
              if (ncr.reason !== updatedValues.reason) {
                updateVariables.reason = updatedValues.reason;
              }
              if (ncr.is_active !== updatedValues.is_active) {
                updateVariables.is_active = updatedValues.is_active;
              }

              console.log(98569856, updatedValues);

              updateNCR({
                variables: {
                  where: {
                    id: ncr.id,
                  },
                  data: {
                    ...updateVariables,
                  },
                },
              });

              setSubmitting(false);
            } catch (e) {
              console.log(e);
            }
          }}
        >
          {({
            handleSubmit,
            isSubmitting,
            handleBlur,
            handleChange,
            errors,
            values,
            setFieldValue,
          }) => {
            return (
              <User>
                {(data) => {
                  const { role } = data.currentUser;
                  console.log(role);

                  return (
                    <form onSubmit={handleSubmit}>
                      <FormItem
                        key="qa_representative"
                        label="QAD Representative"
                        validateStatus={
                          errors["plant_name"] ? "error" : "validating"
                        }
                        help={errors["plant_name"]}
                        required={true}
                        placeholder="plant"
                      >
                        <Select
                          defaultValue={initialValues.qa_representative}
                          mode="default"
                          disabled={
                            isSubmitting ||
                            data.currentUser.role !== "QA_MANAGER"
                          }
                          name="role"
                          value={values.qa_representative}
                          // showSearch
                          // defaultActiveFirstOption={false}
                          showArrow
                          placeholder="QA Representative"
                          filterOption={false}
                          // onSearch={searchField.search}
                          // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
                          onSelect={(value) => {
                            setFieldValue("qa_representative", value);
                          }}
                        >
                          {users
                            .filter((user) =>
                              // remove system admin from the below list
                              ["QA_OFFICER", "QA_EXECUTIVE"].includes(user.role)
                            )
                            .map((user, i) => {
                              return (
                                <Option value={user.id} key={i}>
                                  {user.first_name}
                                </Option>
                              );
                            })}
                        </Select>
                        <h3>Wil be selected by QA Manager</h3>
                      </FormItem>

                      <FormItem
                        key="production_representative"
                        label="Production Representative"
                        validateStatus={
                          errors["plant_name"] ? "error" : "validating"
                        }
                        help={errors["plant_name"]}
                        placeholder="plant"
                        required={true}
                      >
                        <Select
                          mode="default"
                          disabled={
                            isSubmitting ||
                            data.currentUser.role !== "PRODUCTION_MANAGER"
                          }
                          name="role"
                          value={values.production_representative}
                          // showSearch
                          // defaultActiveFirstOption={false}
                          showArrow
                          placeholder="Production representative"
                          filterOption={false}
                          // onSearch={searchField.search}
                          // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
                          onSelect={(value) => {
                            setFieldValue("production_representative", value);
                          }}
                        >
                          {users
                            .filter((user) =>
                              // remove system admin from the below list
                              ["PRODUCTION_OFFICER"].includes(user.role)
                            )
                            .map((user, i) => {
                              return (
                                <Option value={user.id} key={i}>
                                  {user.first_name}
                                </Option>
                              );
                            })}
                        </Select>
                        <h3> Will be selected by Production Manager </h3>
                      </FormItem>

                      <FormItem
                        key="reason"
                        label="Reason For Defect"
                        // validateStatus={errors["plant_name"] ? "error" : "validating"}
                        // help={errors["plant_name"]}
                        required={true}
                        placeholder="plant"
                      >
                        <TextArea
                          rows={4}
                          // disabled={true}
                          value={values.reason}
                          disabled={
                            data.currentUser.id !==
                            ncr.production_representative
                          }
                          onChange={(event) => {
                            setFieldValue("reason", event.target.value);
                          }}
                        />
                        <h3>
                          Will be filled by the selected production
                          representative
                        </h3>
                      </FormItem>

                      <FormItem
                        key="is_active"
                        label="is_active"
                        placeholder="is_active"
                      >
                        <Checkbox
                          name="is_active"
                          disabled={data.currentUser.role !== "QA_MANAGER"}
                          checked={values && values.is_active}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          Checkbox
                        </Checkbox>
                      </FormItem>

                      <FormItem
                        wrapperCol={{
                          xs: { span: 24, offset: 0 },
                          sm: { span: 6, offset: 3 },
                          xl: { span: 6, offset: 2 },
                        }}
                      >
                        <Button
                          type="primary"
                          htmlType="submit"
                          disabled={
                            isSubmitting ||
                            ![
                              "QA_MANAGER",
                              "QA_EXECUTIVE",
                              "PRODUCTION_MANAGER",
                            ].includes(data.currentUser.role)
                          }
                          className="session-form-button"
                          loading={isSubmitting}
                          //icon={<PlusOutlined />}
                        >
                          {isUpdate ? "Update" : "Create"} NCR
                        </Button>
                      </FormItem>
                    </form>
                  );
                }}
              </User>
            );
          }}
        </Formik>
      </NCRWrapper>
    );
  }
};

export default NCRForm;
