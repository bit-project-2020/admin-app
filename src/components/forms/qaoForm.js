import React from "react";
import { Formik } from "formik";

import { Button, Form as AntForm, Input, Checkbox } from "antd";
import styled from "styled-components";

const FormItem = AntForm.Item;

const SizeFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const QAOForm = ({
  log
}) => {

  return (
    <SizeFormWrapper>
      <Formik
        enableReinitialize={true}
        validateOnBlur={false}
        validateOnChange={false}
        onSubmit={async (values, { setSubmitting }) => {
          console.log(100100, values);
        }}
      >
        {({
          handleSubmit
        }) => {
          return (
            <form onSubmit={handleSubmit}>
              <FormItem
                key="size_name"
                label="size_name"
              >
                <Input
                  type="text"
                  name="size_name"
                />
              </FormItem>
              <FormItem
                key="is_active"
                label="is_active"
                required={true}
                placeholder="is_active"
              >
                <Checkbox
                  name="is_active"
                >
                  Checkbox
                </Checkbox>
              </FormItem>

              <FormItem
                wrapperCol={{
                  xs: { span: 24, offset: 0 },
                  sm: { span: 6, offset: 3 },
                  xl: { span: 6, offset: 2 },
                }}
              >
                <Button
                  type="primary"
                >
                  OK
                </Button>
              </FormItem>
            </form>
          );
        }}
      </Formik>
    </SizeFormWrapper>
  );
};

export default QAOForm;
