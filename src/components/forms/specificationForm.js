import React from "react";
import { Formik } from "formik";

import { Button, Form as AntForm, Checkbox, Switch, InputNumber } from "antd";

import styled from "styled-components";

import * as Yup from "yup";

const SpecificationFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }

  span.modal-label {
    min-width: 113px;
    display: inline-block;
    text-align: right;
    font-weight: bold;
    margin-right: 0.7rem;
  }

  h2.modal-label-title {
    margin-bottom: 0px;
  }
`;

const NumberTypeValidationSchema = Yup.object().shape({
  "min-value": Yup.number()
    .required("Minimum value is a required")
    .typeError("Minimum value is required")
    .lessThan(
      Yup.ref("max-value"),
      "Minimum value should be less than the maximum value"
    )
    .nullable(false),

  "max-value": Yup.number()
    .required("Maximum value is a required")
    .typeError("Maximum value is required")
    .moreThan(
      Yup.ref("min-value"),
      "maximum-value should be greater than the minimum-value"
    )
    .nullable(false),
});

const FormItem = AntForm.Item;

const SpecificationForm = ({
  isUpdate,
  initialValues,
  product,
  updateFunction,
  createFunction,
}) => {
  // initial values use to populate the form
  //console.log(400400, initialValues);
  if (!isUpdate) {
    initialValues = { ...initialValues };
    initialValues.product = product.id;
    initialValues.is_active = true;
    if (initialValues.parameter_type === "BOOLEAN") {
      initialValues.value = true;
    }
  } else {
    // handle update
    if (initialValues.parameter_type === "BOOLEAN") {
      initialValues.value = initialValues.spec.value === "true" ? true : false;
      initialValues.is_active = initialValues.spec.is_active;
    } else if (initialValues.parameter_type === "NUMBER") {
      initialValues["min-value"] = parseFloat(
        initialValues.spec.value.split(",")[0]
      );
      initialValues["max-value"] = parseFloat(
        initialValues.spec.value.split(",")[1]
      );
      initialValues.is_active = initialValues.spec.is_active;
    } else if (initialValues.parameter_type === "NUMBER_EXACT") {
      initialValues.value = initialValues.spec.value;
      initialValues.is_active = initialValues.spec.is_active;
    }
    console.log(450652, initialValues);
  }

  return (
    <SpecificationFormWrapper>
      <Formik
        enableReinitialize={true}
        validateOnBlur={true}
        validateOnChange={false}
        initialValues={initialValues}
        validationSchema={
          initialValues.parameter_type === "NUMBER"
            ? NumberTypeValidationSchema
            : null
        }
        onSubmit={async (values, { setSubmitting }) => {
          console.log(100100, initialValues.parameter_type);
          try {
            setSubmitting(true);

            if (isUpdate) {
              const processedData = {};
              processedData.is_active = values.is_active;

              // handle data fields
              if (initialValues.parameter_type === "BOOLEAN") {
                processedData.value = values.value.toString();
              } else if (initialValues.parameter_type === "NUMBER") {
                processedData.value = `${values[
                  "min-value"
                ].toString()},${values["max-value"].toString()}`;
              } else if (initialValues.parameter_type === "NUMBER_EXACT") {
                processedData.value = values.value.toString();
              }

              console.log(600300, processedData);

              updateFunction({
                variables: {
                  where: {
                    id: parseInt(initialValues.spec.id),
                  },
                  data: {
                    ...processedData,
                  },
                },
              });
            } else {
              const updatedData = {
                product: product.id,
                plant_parameter: values.plant_parameter,
                is_active: values.is_active,
                is_default: values.sectionTitle === "General" ? true : false,
              };

              if (initialValues.parameter_type === "BOOLEAN") {
                updatedData.value = values.value.toString();
              } else if (initialValues.parameter_type === "NUMBER") {
                updatedData.value = `${values["min-value"]},${values["max-value"]}`;
              } else if (initialValues.parameter_type === "NUMBER_EXACT") {
                updatedData.value = values.value.toString();
              }

              // if sectionTitle is not General then push the size
              if (values.sectionTitle !== "General") {
                updatedData.size = values.size;
              }

              console.log(650200, updatedData);

              await createFunction({
                variables: {
                  data: {
                    ...updatedData,
                  },
                },
              });
            }

            setSubmitting(false);
          } catch (e) {
            console.log(e);
          }
        }}
      >
        {({
          handleSubmit,
          isSubmitting,
          handleBlur,
          handleChange,
          errors,
          values,
          setErrors,
          setFieldValue,
          touched,
        }) => {
          return (
            <form onSubmit={handleSubmit}>
              {/* display the product and the parameter here */}
              <h2 className="modal-label-title">
                <span className="modal-label"> Product: </span>
                {product.name}
              </h2>
              <h2 className="modal-label-title">
                <span className="modal-label"> Size:</span>
                {initialValues.sectionTitle}
              </h2>
              <h2 className="modal-label-title">
                <span className="modal-label"> Parameter:</span>
                {initialValues.parameter}
              </h2>

              <br />
              {initialValues.parameter_type === "BOOLEAN" && (
                <FormItem
                  key="value"
                  label="value"
                  required={true}
                  placeholder="value"
                >
                  <div className="form-row">
                    <Switch
                      name="specification_value"
                      size="small"
                      checked={values["value"]}
                      onChange={(value) => {
                        setFieldValue("value", value);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </FormItem>
              )}
              {initialValues.parameter_type === "NUMBER" && (
                <>
                  <FormItem
                    key="min-value"
                    label="Minimum value"
                    required={true}
                    placeholder="value"
                    help={errors["min-value"]}
                    validateStatus={
                      errors["min-value"] ? "error" : "validating"
                    }
                  >
                    <div className="form-row">
                      <InputNumber
                        name="min-value"
                        required={true}
                        value={values["min-value"]}
                        onBlur={handleBlur}
                        onChange={(value) => {
                          setFieldValue("min-value", value);
                        }}
                        placeholder={"min-value"}
                      />
                      &nbsp;
                      {values.unit}
                    </div>
                  </FormItem>

                  <FormItem
                    key="max-value"
                    label="Maximum value"
                    required={true}
                    placeholder="value"
                    help={errors["max-value"]}
                    validateStatus={
                      errors["max-value"] ? "error" : "validating"
                    }
                  >
                    <div className="form-row">
                      <InputNumber
                        name="max-value"
                        required={true}
                        value={values["max-value"]}
                        onBlur={handleBlur}
                        onChange={(value) => {
                          setFieldValue("max-value", value);
                        }}
                        placeholder={"max-value"}
                      />
                      &nbsp;
                      {values.unit}
                    </div>
                  </FormItem>
                </>
              )}

              {initialValues.parameter_type === "NUMBER_EXACT" && (
                <>
                  <FormItem
                    key="value"
                    label="value"
                    required={true}
                    placeholder="value"
                  >
                    <div className="form-row">
                      <InputNumber
                        name="value"
                        required={true}
                        value={values["value"]}
                        onBlur={handleBlur}
                        onChange={(value) => {
                          setFieldValue("value", value);
                        }}
                        placeholder={"value"}
                      />
                      {values.unit}
                    </div>
                  </FormItem>
                </>
              )}

              <FormItem
                key="is_active"
                label="is_active"
                required={true}
                placeholder="is_active"
              >
                <Checkbox
                  name="is_active"
                  checked={values && values.is_active}
                  defaultChecked={values.is_active}
                  onChange={handleChange}
                >
                  Checkbox
                </Checkbox>
              </FormItem>
              <FormItem
                wrapperCol={{
                  xs: { span: 24, offset: 0 },
                  sm: { span: 6, offset: 3 },
                  xl: { span: 6, offset: 2 },
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                >
                  {isUpdate ? "Update" : "Create"} Specification
                </Button>
              </FormItem>
            </form>
          );
        }}
      </Formik>
    </SpecificationFormWrapper>
  );
};

export default SpecificationForm;
