import React from "react"; // , { useState }
import { Formik } from "formik";
import { PLANTS_QUERY } from "../../queries/plants";
import { useQuery } from "@apollo/react-hooks";
import styled from "styled-components";

import { Button, Form as AntForm, Input, Upload, Checkbox } from "antd";

import ImgCrop from "antd-img-crop";
import uploadFile from "../../lib/uploadFile";

import * as Yup from "yup";

const PlantFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const FormItem = AntForm.Item;

const PlantFormSchema = Yup.object().shape({
  plant_name: Yup.string()
    .min(1, "Too Short!")
    .max(15, "Too Long plant name!")
    .test("contain-spaces", "spaces are not allowed", (value) => {
      if (value !== undefined && value !== null) {
        if (typeof value === "string") {
          return !value.includes(" ");
        }
      } else {
        return true;
      }
    })
    .required("Plant name is a required field"),
  // is_active: Yup.boolean().required("a value is expected"),
});

const PlantForm = ({
  isUpdate,
  initialValues,
  updateFunction,
  createFunction,
}) => {
  console.log(121212, initialValues);
  const {
    loading,
    error,
    data,
    // refetch
  } = useQuery(PLANTS_QUERY);

  if (loading) {
    return <div>loading</div>;
  }
  if (error) {
    return <div> Error </div>;
  }
  if (data) {
    // use this plants query data to check if there is already a plant for given name
    // const { plants } = data;
    return (
      <PlantFormWrapper>
        <Formik
          validateOnBlur={true}
          validateOnChange={false}
          initialValues={{ ...initialValues }}
          validationSchema={PlantFormSchema}
          onSubmit={async (values, { setSubmitting }) => {
            console.log(600150, values);
            try {
              const updatedValues = { ...values };

              setSubmitting(true);

              if (
                // if there is a file and there is no url you have to upload it to get a url
                values["image_url"] &&
                values["image_url"].length !== 0 &&
                !values["image_url"][0].url
              ) {
                console.log(78989, values["image_url"]);
                updatedValues["image_url"] = await uploadFile({
                  file: values["image_url"][0].originFileObj,
                  uploadPreset: "plant_photos",
                });
              } else if (
                values["image_url"] &&
                values["image_url"].length === 0 // if added the image then delete before click on create
              ) {
                delete updatedValues.image_url;
              } else {
                // updatedValues["image_url"] = null;
              }

              if (isUpdate) {
                // delete conflicting fields
                delete updatedValues.id;
                delete updatedValues.plant_name;
                delete updatedValues.__typename;

                console.log(98569856, updatedValues);

                // in the update if the image_url length is 0 set null
                if (values["image_url"] && values["image_url"].length === 0) {
                  updatedValues.image_url = null;
                }

                updateFunction({
                  variables: {
                    where: {
                      plant_name: initialValues.plant_name,
                    },
                    data: {
                      ...updatedValues,
                    },
                  },
                });
              } else {
                // console.log(600200100, updatedValues);

                await createFunction({
                  variables: {
                    data: {
                      ...updatedValues,
                    },
                  },
                });
              }

              setSubmitting(false);
            } catch (e) {
              console.log(e);
            }
          }}
        >
          {({
            handleSubmit,
            isSubmitting,
            handleBlur,
            handleChange,
            errors,
            values,
            setFieldValue,
          }) => {
            console.log(7777, values["image_url"]);
            return (
              <form onSubmit={handleSubmit}>
                <FormItem
                  key="plant_name"
                  label="Plant Name"
                  validateStatus={errors["plant_name"] ? "error" : "validating"}
                  help={errors["plant_name"]}
                  required={isUpdate === false}
                  placeholder="plant"
                >
                  <Input
                    name="plant_name"
                    disabled={isUpdate}
                    placeholder="Plant Name"
                    value={values.plant_name}
                    onBlur={handleBlur}
                    onChange={handleChange}
                  />
                </FormItem>

                <FormItem
                  key="is_active"
                  label="is_active"
                  // required={isUpdate === false}
                  placeholder="is_active"
                >
                  <Checkbox
                    name="is_active"
                    checked={values && values.is_active}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    Checkbox
                  </Checkbox>
                </FormItem>

                <FormItem
                  key="Image Url"
                  label="Image Url"
                  // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                  // help={errors[formItem.name]}
                  required={false}
                >
                  <ImgCrop>
                    <Upload
                      maxCount={1}
                      name="image_url"
                      // adding a falty url to prevent automatic uploading
                      action="https://www.nowhereisherethere.io/v2/5cc8019d300000980a055e76"
                      accept="image/*"
                      listType="picture-card"
                      fileList={values["image_url"]}
                      onChange={({ fileList }) => {
                        console.log(54612, fileList.slice(-1));
                        setFieldValue("image_url", fileList.slice(-1));
                      }}

                      // onPreview={onPreview}
                    >
                      {"+ Upload"}
                    </Upload>
                  </ImgCrop>
                </FormItem>

                <FormItem
                  wrapperCol={{
                    xs: { span: 24, offset: 0 },
                    sm: { span: 6, offset: 3 },
                    xl: { span: 6, offset: 2 },
                  }}
                >
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={isSubmitting}
                    className="session-form-button"
                    loading={isSubmitting}
                    //icon={<PlusOutlined />}
                  >
                    {isUpdate ? "Update" : "Create"} Plant
                  </Button>
                </FormItem>
              </form>
            );
          }}
        </Formik>
      </PlantFormWrapper>
    );
  }
};

export default PlantForm;
