import React from "react"; // , { useState }
import { Formik } from "formik";
import { PLANTS_QUERY } from "../../queries/plants";
import { useQuery } from "@apollo/react-hooks";
import styled from "styled-components";

import { Button, Form as AntForm, Select, Input, Upload } from "antd";

import ImgCrop from "antd-img-crop";
import uploadFile from "../../lib/uploadFile";
import * as Yup from "yup";

const ProductFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const ProductFormSchema = Yup.object().shape({
  name: Yup.string()
    .min(1, "Too Short!")
    .max(20, "Too Long Product name!")
    // .test("contain-spaces", "spaces are not allowed", (value) => {
    //   if (value !== undefined && value !== null) {
    //     if (typeof value === "string") {
    //       return !value.includes(" ");
    //     }
    //   } else {
    //     return true;
    //   }
    // })
    .required("Product name is a required field"),
});

const FormItem = AntForm.Item;

const { Option } = Select;

const ProductForm = ({
  isUpdate,
  initialValues,
  updateFunction,
  createFunction,
}) => {
  console.log(121212, initialValues);
  const {
    loading,
    error,
    data,
    // refetch
  } = useQuery(PLANTS_QUERY);

  if (loading) {
    return <div>loading</div>;
  }
  if (error) {
    return <div> Error </div>;
  }
  if (data) {
    const { plants } = data;
    return (
      <ProductFormWrapper>
        <Formik
          validateOnBlur={true}
          validateOnChange={false}
          initialValues={{ ...initialValues }}
          validationSchema={ProductFormSchema}
          onSubmit={async (values, { setSubmitting }) => {
            try {
              const updatedValues = { ...values };

              setSubmitting(true);

              if (
                values["image_url"] &&
                values["image_url"].length !== 0 &&
                !values["image_url"][0].url
              ) {
                console.log(78989, values["image_url"]);
                updatedValues["image_url"] = await uploadFile({
                  file: values["image_url"][0].originFileObj,
                  uploadPreset: "product_photos",
                });
              } else if (
                values["image_url"] &&
                values["image_url"].length === 0 // if added the image then delete before click on create
              ) {
                delete updatedValues.image_url;
              } else {
                // updatedValues["image_url"] = null;
              }

              if (isUpdate) {
                // delete conflicting fields
                delete updatedValues.id;
                delete updatedValues.__typename;

                console.log(98569856, updatedValues);

                // in the update if the image_url length is 0 set null
                if (values["image_url"] && values["image_url"].length === 0) {
                  updatedValues.image_url = null;
                }

                console.log(600100200, updatedValues);

                updateFunction({
                  variables: {
                    where: {
                      id: parseInt(initialValues.id),
                    },
                    data: {
                      ...updatedValues,
                    },
                  },
                });
              } else {
                await createFunction({
                  variables: {
                    data: {
                      ...updatedValues,
                    },
                  },
                });
                // run create function
              }

              setSubmitting(false);
            } catch (e) {
              console.log(e);
            }
          }}
        >
          {({
            handleSubmit,
            isSubmitting,
            handleBlur,
            handleChange,
            errors,
            values,
            setFieldValue,
          }) => {
            console.log(7777, values["image_url"]);
            return (
              <form onSubmit={handleSubmit}>
                <FormItem
                  key="plant"
                  // labelCol={{
                  //   xs: { span: 24 },
                  //   sm: { span: 3 },
                  //   xl: { span: 2 },
                  // }}
                  // wrapperCol={{ xs: { span: 24 }, sm: { span: 8 } }}
                  label="Plant"
                  // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                  // help={errors[formItem.name]}
                  required={true}
                  placeholder="plant"
                >
                  <Select
                    mode="default"
                    disabled={isSubmitting || isUpdate}
                    value={values["plant"]}
                    name="plant"
                    // showSearch
                    defaultActiveFirstOption={false}
                    showArrow
                    placeholder="plant"
                    filterOption={false}
                    // onSearch={searchField.search}
                    // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
                    onSelect={(value) => {
                      setFieldValue("plant", value);
                    }}
                  >
                    {plants.map((plant) => {
                      // console.log(1111,plant);
                      return (
                        <Option value={plant.plant_name} key={plant.id}>
                          {plant.plant_name}
                        </Option>
                      );
                    })}
                  </Select>
                </FormItem>

                <FormItem
                  // labelCol={{
                  //   xs: { span: 24 },
                  //   sm: { span: 3 },
                  //   xl: { span: 2 },
                  // }}
                  // wrapperCol={{ xs: { span: 24 }, sm: { span: 8 } }}
                  label="Product Name"
                  validateStatus={errors["name"] ? "error" : "validating"}
                  help={errors["name"]}
                  required={true}
                  placeholder="plant"
                >
                  <Input
                    name="name"
                    placeholder="Product Name"
                    value={values.name}
                    onChange={handleChange}
                  />
                </FormItem>

                <FormItem
                  key="Image Url"
                  label="Image Url"
                  // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                  // help={errors[formItem.name]}
                  required={false}
                >
                  <ImgCrop>
                    <Upload
                      maxCount={1}
                      name="image_url"
                      accept="image/*"
                      listType="picture-card"
                      fileList={values["image_url"]}
                      onChange={({ fileList }) => {
                        console.log(54612, fileList.slice(-1));
                        setFieldValue("image_url", fileList.slice(-1));
                      }}

                      // onPreview={onPreview}
                    >
                      {"+ Upload"}
                    </Upload>
                  </ImgCrop>
                </FormItem>

                <FormItem
                  wrapperCol={{
                    xs: { span: 24, offset: 0 },
                    sm: { span: 6, offset: 3 },
                    xl: { span: 6, offset: 2 },
                  }}
                >
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={isSubmitting}
                    className="session-form-button"
                    loading={isSubmitting}
                    //icon={<PlusOutlined />}
                  >
                    {isUpdate ? "Update" : "Create"} Product
                  </Button>
                </FormItem>
              </form>
            );
          }}
        </Formik>
      </ProductFormWrapper>
    );
  }
};

export default ProductForm;
