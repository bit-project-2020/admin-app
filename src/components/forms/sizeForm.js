import React from "react";
import { Formik } from "formik";

import { Button, Form as AntForm, Input, Checkbox } from "antd";
import styled from "styled-components";

const FormItem = AntForm.Item;

const SizeFormWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const SizeForm = ({
  isUpdate,
  initialValues,
  product,
  updateFunction,
  createFunction,
}) => {
  if (!isUpdate) {
    initialValues = {};
    initialValues.is_active = true;
  }

  return (
    <SizeFormWrapper>
      <Formik
        enableReinitialize={true}
        validateOnBlur={false}
        validateOnChange={false}
        initialValues={initialValues}
        // validationSchema={schema.validationSchema}
        onSubmit={async (values, { setSubmitting }) => {
          console.log(100100, values);
          try {
            const updatedValues = { ...values };
            setSubmitting(true);

            if (isUpdate) {
              // delete conflicting fields
              delete updatedValues.id;
              delete updatedValues.__typename;
              delete updatedValues.product;

              console.log(98569856, updatedValues);

              updateFunction({
                variables: {
                  where: {
                    id: parseInt(initialValues.id),
                  },
                  data: {
                    ...updatedValues,
                  },
                },
              });
            } else {
              await createFunction({
                variables: {
                  data: {
                    product: product,
                    ...updatedValues,
                  },
                },
              });
              // run create function
            }

            setSubmitting(false);
          } catch (e) {
            console.log(e);
          }
        }}
      >
        {({
          handleSubmit,
          isSubmitting,
          handleBlur,
          handleChange,
          errors,
          values,
          setFieldValue,
          touched,
        }) => {
          return (
            <form onSubmit={handleSubmit}>
              <FormItem
                key="size_name"
                label="size_name"
                // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                // help={errors[formItem.name]}
                required={true}
                placeholder="size_name"
              >
                <Input
                  type="text"
                  name="size_name"
                  onChange={(e) => {
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  value={values && values.size_name}
                />
              </FormItem>
              <FormItem
                key="is_active"
                label="is_active"
                required={true}
                placeholder="is_active"
              >
                <Checkbox
                  name="is_active"
                  checked={values && values.is_active}
                  onChange={handleChange}
                >
                  Checkbox
                </Checkbox>
              </FormItem>

              <FormItem
                wrapperCol={{
                  xs: { span: 24, offset: 0 },
                  sm: { span: 6, offset: 3 },
                  xl: { span: 6, offset: 2 },
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                  //icon={<PlusOutlined />}
                >
                  {isUpdate ? "Update" : "Create"} Size
                </Button>
              </FormItem>
            </form>
          );
        }}
      </Formik>
    </SizeFormWrapper>
  );
};

export default SizeForm;
