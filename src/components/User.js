import React from "react";
import { useQuery } from "@apollo/react-hooks";

import LoadingScreen from "./LoadingScreen";

import { CURRENT_USER_QUERY } from "../queries/currentUser";

import avatarImage from "../assets/images/avatar.jpg";

const User = (props) => {
  const { loading, error, data } = useQuery(CURRENT_USER_QUERY);

  // if (loading) return <p>Loading...</p>;
  if (loading) return <LoadingScreen />;
  if (error) {
    console.log(error);
  }
  if (data) {
    // console.log(100100, data);
    if (data.currentUser && data.currentUser.profile_photo === null) {
      data.currentUser.profile_photo = avatarImage;
    }

    return <> {props.children(data)} </>;
  }
};

export default User;
