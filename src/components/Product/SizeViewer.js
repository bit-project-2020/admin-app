import styled from "styled-components";
import React from "react";
import { EditOutlined } from "@ant-design/icons";

import { Card } from "antd";

const SizeManagerWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  .active {
    color: green;
  }

  .inactive {
    color: red;
  }
`;

// use this component to show sizes
const SizeViewer = ({ sizes, setInitialView, setTheItemToUpdate }) => {
  return (
    <SizeManagerWrapper>
      {sizes.map((size, id) => {
        return (
          <Card
            key={size.id}
            style={{ width: 200, margin: "10px 10px", textAlign: "center" }}
            actions={[
              <EditOutlined
                key="setting"
                onClick={() => {
                  setTheItemToUpdate(size);
                  setInitialView("update");
                }}
              />,
            ]}
          >
            <h2> {size.size_name} </h2>
            <h3>
              {size.is_active ? (
                <div className="active">Active</div>
              ) : (
                <div className="inactive">Inactive</div>
              )}
            </h3>
          </Card>
        );
      })}
    </SizeManagerWrapper>
  );
};

export default SizeViewer;
