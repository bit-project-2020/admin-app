/* eslint-disable eqeqeq */
import React from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Table, Checkbox } from "antd";
import showNotifications from "../../lib/showNotifications";
import { PLANT_PARAMETERS_QUERY } from "../../queries/plantParameters";
import { DEVIATIONS_QUERY } from "../../queries/deviations";
import { SET_DEVIATION_MUTATION } from "../../mutations/SetDeviation";
import { UPDATE_DEVIATION_MUTATION } from "../../mutations/UpdateDeviation";

const columns = (sizes, setDeviation, updateDeviation, product_id) => {
  const cols = [
    {
      title: "Parameter Name",
      dataIndex: "parameter",
      key: "parameter",
    },
  ];
  // loop through the params and select its size list
  sizes.forEach((size) => {
    const sizeColumn = {
      title: size.size_name,
      dataIndex: size.size_name,
      key: `${size.id}${size.size_name}`,
      render: (value, record, rowIndex) => {
        // console.log(789878987, value, record, rowIndex);
        return (
          <Checkbox
            checked={value[0]}
            onClick={() => {
              // if the deviation id is null
              if (value[3] == null) {
                setDeviation({
                  variables: {
                    data: {
                      product: product_id,
                      plant_parameter: value[1],
                      size: value[2],
                      is_active: !value[0],
                    },
                  },
                });
              } else {
                // run the update mutation by providing its id
                updateDeviation({
                  variables: {
                    data: {
                      is_active: !value[0],
                    },
                    where: {
                      id: value[3],
                    },
                  },
                });
              }
            }}
          />
        );
      },
    };
    cols.push(sizeColumn);
  });

  return cols;
};

function DeviationManager(props) {
  console.log(7898, props);

  const { loading, error, data: plantParameters } = useQuery(
    PLANT_PARAMETERS_QUERY,
    {
      variables: {
        where: {
          plant: props.product.product.plant,
        },
      },
    }
  );

  const {
    // loading: deviationsLoading,
    // error: deviationsError,
    data: deviationsData,
  } = useQuery(DEVIATIONS_QUERY, {
    variables: {
      product_id: props.product.product.id,
    },
  });

  // this will replace
  const [updateDeviation] = useMutation(UPDATE_DEVIATION_MUTATION, {
    onCompleted: () => {
      showNotifications({
        message: "Product Specification deviation updated succesfully!",
      });
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: DEVIATIONS_QUERY,
        variables: {
          product_id: props.product.product.id,
        },
      },
    ],
  });

  const [setDeviation] = useMutation(SET_DEVIATION_MUTATION, {
    onCompleted: () => {
      showNotifications({
        message: "Product specification deviation updated succesfully!",
      });
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    // change with teh deviations query
    refetchQueries: [
      {
        query: DEVIATIONS_QUERY,
        variables: {
          product_id: props.product.product.id,
        },
      },
    ],
  });

  if (loading) {
    return <h1> I'm loading </h1>;
  }

  if (error) {
    console.log(33551122, error);
    return <h2>error</h2>;
  }

  let sizes = [];
  if (props.sizes) {
    sizes = props.sizes.sizes;
  }

  const { id: product_id } = props.product.product;

  // prpare the data set and columns here
  const data = [];
  let parameters;
  if (plantParameters && deviationsData) {
    const { deviations } = deviationsData;
    parameters = plantParameters.plantParameters.filter((parameter) =>
      ["NUMBER", "NUMBER_EXACT", "BOOLEAN"].includes(parameter.parameter_type)
    );

    parameters.forEach((param) => {
      const paramRaw = {};
      paramRaw.parameter = param.parameter;
      sizes.forEach((size) => {
        const deviation = deviations.find((deviation) => {
          return (
            deviation.plant_parameter == param.id && deviation.size == size.id
          );
        });

        // if the field has a value get it here
        paramRaw[size.size_name] = [
          (deviation && deviation.is_active) || false,
          param.id,
          size.id,
          (deviation && deviation.id) || null,
        ];
      });
      data.push(paramRaw);
    });
  }

  if (data) {
    return (
      <div>
        <Table
          dataSource={data}
          columns={columns(sizes, setDeviation, updateDeviation, product_id)}
          rowKey="parameter"
        />
      </div>
    );
  }
}

export default DeviationManager;
