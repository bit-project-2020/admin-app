import React, { useState } from "react";
import { Button, Modal } from "antd";
import { useMutation, useQuery } from "@apollo/react-hooks";
import showNotifications from "../../lib/showNotifications";
import { SIZES_QUERY } from "../../queries/sizes";
import { UPDATE_SIZE_MUTATION } from "../../mutations/UpdateSize";
import { CREATE_SIZE_MUTATION } from "../../mutations/CreateSize";
import SizeViewer from "../../components/Product/SizeViewer";

import SizeForm from "../forms/sizeForm";

function SizeManager(props) {
  const product_id = props.id;
  const [initialView, setInitialView] = useState("view");
  const [theItemToUpdate, setTheItemToUpdate] = useState(null);

  const { loading, error, data } = useQuery(SIZES_QUERY, {
    variables: {
      where: {
        product_id: props.id,
      },
    },
  });

  const [createSize] = useMutation(CREATE_SIZE_MUTATION, {
    onCompleted: () => {
      setTheItemToUpdate(null);
      showNotifications({ message: "Product Size succesfully created!" });
      setInitialView("view");
      // have to close the modal
    },
    onError: () => {
      setTheItemToUpdate(null);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: SIZES_QUERY,
        variables: {
          where: {
            product_id,
          },
        },
      },
    ],
  });

  const [updateSize] = useMutation(UPDATE_SIZE_MUTATION, {
    onCompleted: () => {
      setTheItemToUpdate(null);
      showNotifications({ message: "Size successfully updated!" });
      setInitialView("view");
      console.log(9964, theItemToUpdate);
    },
    onError: () => {
      setTheItemToUpdate(null);
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: SIZES_QUERY,
        variables: {
          where: {
            product_id: product_id,
          },
        },
      },
    ],
  });

  console.log(data);
  if (loading) {
    return <h1> loading </h1>;
  }

  if (error) {
    return <h1> Error </h1>;
  }

  if (data) {
    console.log(200200, theItemToUpdate);
    return (
      <div>
        <Button
          style={{ float: "right" }}
          onClick={() => {
            setInitialView("create");
          }}
        >
          Create New Size
        </Button>
        <SizeViewer
          setInitialView={setInitialView}
          sizes={data.sizes}
          setTheItemToUpdate={setTheItemToUpdate}
          initialView={initialView}
        />
        <Modal
          title={`${initialView === "update" ? "Update" : "Create"} size`}
          visible={initialView === "create" || initialView === "update"}
          onCancel={() => {
            setInitialView("view");
            setTheItemToUpdate(null);
          }}
          footer={null}
        >
          <SizeForm
            isUpdate={initialView === "update"}
            product={product_id}
            initialValues={theItemToUpdate}
            createFunction={createSize}
            updateFunction={updateSize}
          />
        </Modal>
      </div>
    );
  }
}

export default SizeManager;
