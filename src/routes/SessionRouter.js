import React from "react";
import {
  BrowserRouter as ReactRouter,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import SessionLayout from "../containers/SessionLayout";
import ResetPassword from "../containers/ResetPassword";

import Login from "../containers/Login";

const Router = () => (
  <ReactRouter>
    <div>
      <SessionLayout>
        <Switch>
          <Route path="/login" exact component={Login} />
          <Route path="/reset-password" exact component={ResetPassword} />
          <Redirect to="/login" push />
        </Switch>
      </SessionLayout>
    </div>
  </ReactRouter>
);

export default Router;
