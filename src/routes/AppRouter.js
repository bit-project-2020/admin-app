import React from "react";
import {
  BrowserRouter as ReactRouter,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import AppLayout from "../containers/AppLayout";

import Dashboard from "../containers/Dashboard";

import Parameters from "../containers/Parameters";
import AddParameter from "../containers/AddParameter";

import PlantParameters from "../containers/ManagePlantParameters";
import AddPlantParameter from "../containers/AddPlantParameter";

import Plants from "../containers/Plants";
import AddPlant from "../containers/AddPlant";
import UpdatePlant from "../containers/UpdatePlant";

import Users from "../containers/Users";
import AddUser from "../containers/AddUser";

import Products from "../containers/Products";
import AddProduct from "../containers/AddProduct";
import UpdateProduct from "../containers/UpdateProduct";

import AddNewLog from "../containers/AddNewLog";

import ManageProduct from "../containers/ManageProduct";

import DefectSeverity from "../containers/reports/DefectSeverity";
import NCRCount from "../containers/reports/NCRCount";
import UpdateUser from "../containers/UpdateUser";

import InspectionData from "../containers/InspectionData";
import InspectionRecord from "../containers/InspectionDataOne";

import QAOLogs from '../containers/QAOLogs';
import QAODetail from '../containers/QAODetail'
import NCRs from "../containers/NCRs";

import Backups from "../containers/Backups";

const Router = () => (
  <ReactRouter>
    <div>
      <AppLayout>
        <Switch>
          <Route path="/" exact component={Dashboard} />

          <Route path="/plants" exact component={Plants} />
          <Route path="/plants/add" exact component={AddPlant} />
          <Route path="/update_plants/:plant" exact component={UpdatePlant} />

          <Route
            path="/manage_plant_parameters/:plant_name"
            exact
            component={PlantParameters}
          />
          <Route
            path="/add_plant_parameter/:plant_name"
            exact
            component={AddPlantParameter}
          />

          <Route path="/parameters" exact component={Parameters} />
          <Route path="/parameters/add" exact component={AddParameter} />
          {/* <Route path="/plantparams/add" exact component={AddPlantParameter} /> */}

          <Route path="/products" exact component={Products} />
          <Route path="/products/add" exact component={AddProduct} />
          <Route
            path="/products/:product_id/update"
            exact
            component={UpdateProduct}
          />
          {/* <Route
            path="/manage_product_specifications/:product_id/:plant"
            exact
            component={SpecRenderer}
          /> */}

          <Route
            path="/manage_product/:product_id"
            exact
            component={ManageProduct}
          />  

          <Route 
            path="/logs" 
            exact 
            component={QAOLogs} 
          />

          <Route 
            path="/logs/detail" 
            exact 
            component={QAODetail} 
          />


          <Route
            path="/reports/defect-Severity"
            exact
            component={DefectSeverity}
          />
          <Route path="/reports/ncr-count" exact component={NCRCount} />

          <Route path="/logs/add" exact component={AddNewLog} />

          <Route path="/users" exact component={Users} />
          <Route path="/users/add" exact component={AddUser} />
          <Route path="/users/:user_id/update" exact component={UpdateUser} />
          <Route path="/process-data" exact component={InspectionData} />
          <Route path="/packaging-data" exact component={InspectionData} />
          <Route
            path="/:path_name/:plant/:record_id"
            exact
            component={InspectionRecord}
          />
          <Route path="/ncr" exact component={NCRs} />

          <Route path="/backup" exact component={Backups} />

          <Redirect to="/" from="/login" push />
          <Redirect to="/" from="/forgotpassword" push />

          {/* redirect every other path to home */}
          <Redirect to="/" />
        </Switch>
      </AppLayout>
    </div>
  </ReactRouter>
);

export default Router;
