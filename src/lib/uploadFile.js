import axios from "axios";

import { CLOUDINARY_UPLOAD_URL } from "../config";

const uploadFile = async ({ file, uploadPreset, multiSize }) => {
  try {
    const data = new FormData();
    data.append("upload_preset", uploadPreset);
    data.append("file", file);
    data.append("cloud_name", "dula94");

    console.log(444555, data);

    const res = await axios.post(CLOUDINARY_UPLOAD_URL, data);
    console.log(res);

    let imageURL;

    imageURL = res.data.secure_url;
    console.log(356214, imageURL);

    if (!imageURL) {
      throw new Error(`Cloudinary error: ${res.data.eager[0].reason}`);
    }

    return imageURL;
  } catch (error) {
    throw error;
  }
};

export default uploadFile;
