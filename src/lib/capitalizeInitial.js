export default function (word) {
  return word[0].toUpperCase() + word.substring(1);
}
