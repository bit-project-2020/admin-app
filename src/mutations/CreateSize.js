import { gql } from "apollo-boost";

const CREATE_SIZE_MUTATION = gql`
  mutation CreateSize($data: SizeCreateInput!) {
    createSize(data: $data) {
      id
      size_name
      product
      is_active
    }
  }
`;

export { CREATE_SIZE_MUTATION };
