import { gql } from "apollo-boost";

const CREATE_PLANT_MUTATION = gql`
  mutation CreatePlant($data: PlantCreateInput!) {
    createPlant(data: $data) {
      is_active
      plant_name
      image_url
    }
  }
`;
export { CREATE_PLANT_MUTATION };
