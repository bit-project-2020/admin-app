import { gql } from "apollo-boost";

const CREATE_SPECIFICATION = gql`
  mutation create_specification_mutation($data: SpecificationCreateInput!) {
    createSpecification(data: $data) {
      id
      product
      value
      size
      is_default
      is_active
    }
  }
`;
export { CREATE_SPECIFICATION };
