import { gql } from "apollo-boost";

const CREATE_PRODUCT_MUTATION = gql`
  mutation createProduct($data: ProductCreateInput!) {
    createProduct(data: $data) {
      name
      plant
    }
  }
`;

export { CREATE_PRODUCT_MUTATION };
