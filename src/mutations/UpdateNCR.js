import { gql } from "apollo-boost";

const UPDATE_NCR_MUTATION = gql`
  mutation update_NCR_mutation(
    $where: NCRWhereUniqueInput!
    $data: NCRUpdateInput!
  ) {
    updateNCR(where: $where, data: $data) {
      id
      plant
      qa_representative
      production_representative
      comment
      reason
      is_active
    }
  }
`;
export { UPDATE_NCR_MUTATION };
