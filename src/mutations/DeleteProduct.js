import { gql } from "apollo-boost";

const DELETE_PRODUCT_MUTATION = gql`
  mutation deleteProduct($where: ProductWhereUniqueInput!) {
    deleteProduct(where: $where) {
      id
      name
      plant
      is_active
      image_url
    }
  }
`;

export { DELETE_PRODUCT_MUTATION };
