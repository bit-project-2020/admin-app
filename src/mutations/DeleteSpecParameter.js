import { gql } from "apollo-boost";

const DELETE_SPEC_PARAMETER_MUTATION = gql`
  mutation deleteSpecParameter($where: SpecParameterWhereUniqueInput!) {
    deleteSpecParameter(where: $where) {
      name
      parameter_type
      type
    }
  }
`;

export { DELETE_SPEC_PARAMETER_MUTATION };
