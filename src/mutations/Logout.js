import { gql } from "apollo-boost";

const LOGOUT_MUTATION = gql`
  mutation logout {
    logout {
      status
      message
    }
  }
`;

export { LOGOUT_MUTATION };
