import { gql } from "apollo-boost";

export const UPDATE_USER_MUTATION = gql`
  mutation update_user($where: UserWhereUniqueInput!, $data: UserUpdateInput!) {
    updateUser(where: $where, data: $data) {
      id
      first_name
      role
      last_name
      department
      emp_no
      email
      profile_photo
    }
  }
`;
