import { gql } from "apollo-boost";

const UPDATE_SPECIFICATION_MUTATION = gql`
  mutation update_specificatoin_mutation(
    $data: SpecificationUpdateInput!
    $where: SpecificationWhereUniqueInput!
  ) {
    updateSpecification(where: $where, data: $data) {
      size
      is_active
      is_default
    }
  }
`;

export { UPDATE_SPECIFICATION_MUTATION };
