import { gql } from "apollo-boost";

const CREATE_USER_MUTATION = gql`
  mutation CreateUser($data: UserCreateInput!) {
    createUser(data: $data) {
      id
      first_name
      last_name
      role
      department
      emp_no
      user_name
      email
      is_active
    }
  }
`;

export { CREATE_USER_MUTATION };
