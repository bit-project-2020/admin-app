import { gql } from "apollo-boost";

const DELETE_PLANT_PARAMETER = gql`
  mutation delete_plant_parameter($where: PlantParameterWhereUniqueInput!) {
    deletePlantParameter(where: $where) {
      id
    }
  }
`;

export { DELETE_PLANT_PARAMETER };
