import { gql } from "apollo-boost";

const RESTORE_BACKUP_MUTATION = gql`
  mutation restore_backup_mutation($backup_id: String!) {
    restoreBackup(backupID: $backup_id) {
      status
      message
    }
  }
`;
export { RESTORE_BACKUP_MUTATION };
