import { gql } from "apollo-boost";

const CREATE_SPEC_PARAMETER_MUTATION = gql`
  mutation createSpecParameter($data: SpecParameterCreateInput!) {
    createSpecParameter(data: $data) {
      name
      parameter_type
      type
    }
  }
`;

export { CREATE_SPEC_PARAMETER_MUTATION };
