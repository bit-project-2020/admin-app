import { gql } from "apollo-boost";

const DELETE_PLANT_MUTATION = gql`
  mutation deletePlant($plant_name: String!) {
    deletePlant(plant_name: $plant_name) {
      plant_name
      is_active
    }
  }
`;

export { DELETE_PLANT_MUTATION };
