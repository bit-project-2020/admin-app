import { gql } from "apollo-boost";

const UPDATE_PLANT_MUTATION = gql`
  mutation update_plant_mutation(
    $where: PlantWhereUniqueInput!
    $data: PlantUpdateInput!
  ) {
    updatePlant(where: $where, data: $data) {
      image_url
      is_active
      plant_name
    }
  }
`;

export { UPDATE_PLANT_MUTATION };
