import { gql } from "apollo-boost";

const UPDATE_PARAMETER_MUTATION = gql`
  mutation update_parameter_mutation(
    $where: SpecParameterWhereUniqueInput!
    $data: SpecParameterUpdateInput
  ) {
    updateSpecParameter(where: $where, data: $data) {
      is_active
      parameter_type
      name
    }
  }
`;

export { UPDATE_PARAMETER_MUTATION };
