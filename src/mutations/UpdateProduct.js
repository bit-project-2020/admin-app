import { gql } from "apollo-boost";

const UPDATE_PRODUCT_MUTATION = gql`
  mutation UpdateProductMutation(
    $where: ProductWhereUniqueInput!
    $data: ProductUpdateInput!
  ) {
    updateProduct(where: $where, data: $data) {
      id
      plant
      is_active
      name
      image_url
    }
  }
`;

export { UPDATE_PRODUCT_MUTATION };
