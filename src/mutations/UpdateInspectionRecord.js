import { gql } from "apollo-boost";

export const UPDATE_INSPECTION_RECORD_MUTATION = gql`
  mutation update_inspection_record(
    $data: InspectionRecordUpdateInput!
    $where: InspectionRecordWhereInput!
  ) {
    updateInspectionRecord(where: $where, data: $data) {
      status
      message
    }
  }
`;
