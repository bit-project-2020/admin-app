import { gql } from "apollo-boost"

const CREATE_NCR_MUTATION = gql`
  mutation create_ncr_mutation($data: NCRCreateInput!) {
    createNCR(data: $data) {
      id
      created_by
      qa_representative
      deviated_parameters
      reason
      production_representative
      created_at
      plant
      product
      is_active
      size
      type
      comment
      record_id
    }
  }
`
export { CREATE_NCR_MUTATION }
