import { gql } from "apollo-boost";

const UPDATE_PLANT_PARAMETER_MUTATION = gql`
  mutation updatePlantParameter(
    $where: PlantParameterWhereUniqueInput!
    $data: PlantParameterUpdateInput!
  ) {
    updatePlantParameter(where: $where, data: $data) {
      id
      plant
      is_active
      parameter
    }
  }
`;

export { UPDATE_PLANT_PARAMETER_MUTATION };
