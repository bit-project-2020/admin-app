import { gql } from "apollo-boost";

const CREATE_PLANT_PARAMETER_MUTATION = gql`
  mutation createPlantParameter($data: PlantParameterCreateInput!) {
    createPlantParameter(data: $data) {
      id
      plant
      is_active
      parameter
    }
  }
`;

export { CREATE_PLANT_PARAMETER_MUTATION };
