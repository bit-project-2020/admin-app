import { gql } from "apollo-boost";

export const SET_DEVIATION_MUTATION = gql`
  mutation set_deviation($data: DeviationCreateInput!) {
    setDeviation(data: $data) {
      product
    }
  }
`;
