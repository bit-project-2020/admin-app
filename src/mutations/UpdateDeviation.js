import { gql } from "apollo-boost";

export const UPDATE_DEVIATION_MUTATION = gql`
  mutation update_deviation_mutation(
    $where: DeviationWhereUniqueInput!
    $data: DeviationUpdateInput!
  ) {
    updateDeviation(where: $where, data: $data) {
      product
      plant_parameter
      is_active
    }
  }
`;
