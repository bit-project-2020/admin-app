import { gql } from "apollo-boost";

const UPDATE_SIZE_MUTATION = gql`
  mutation UpdateSizeMutation(
    $where: SizeWhereUniqueInput!
    $data: SizeUpdateInput!
  ) {
    updateSize(where: $where, data: $data) {
      id
      size_name
      product
      is_active
    }
  }
`;

export { UPDATE_SIZE_MUTATION };
