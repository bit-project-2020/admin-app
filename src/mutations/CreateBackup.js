import { gql } from "apollo-boost";

const CREATE_BACKUP_MUTATION = gql`
  mutation create_backup_mutation {
    createBackup {
      status
      message
    }
  }
`;
export { CREATE_BACKUP_MUTATION };
