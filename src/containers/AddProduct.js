import React from "react";
import { useMutation } from "@apollo/react-hooks";
import ProductForm from "../components/forms/productForm";

import showNotifications from "../lib/showNotifications";

import { PRODUCTS_QUERY } from "../queries/products";
import { CREATE_PRODUCT_MUTATION } from "../mutations/CreateProduct";

import styled from "styled-components";

const AddProductWrapper = styled.div`
  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;

const AddProduct = ({ history }) => {
  const [
    createProduct,
    // { data, loading, error }
  ] = useMutation(CREATE_PRODUCT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Parameter successfully added!" });
      history.push("/products");
    },
    onError: (error) => {
      console.log(77997799, error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: PRODUCTS_QUERY }],
  });

  return (
    <AddProductWrapper>
      <div className="form-title">Create a new Product</div>
      <ProductForm isUpdate={false} createFunction={createProduct} />
    </AddProductWrapper>
  );
};

export default AddProduct;
