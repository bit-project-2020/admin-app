import React from "react";
import { withRouter } from "react-router-dom";

import { PRODUCT_QUERY } from "../queries/product";
import { UPDATE_PRODUCT_MUTATION } from "../mutations/UpdateProduct";

import { useMutation, useQuery } from "@apollo/react-hooks";
import ProductForm from "../components/forms/productForm";

import showNotifications from "../lib/showNotifications";

const UpdateProduct = ({ match }) => {
  const { product_id } = match.params;

  // get current product
  const { loading, error, data } = useQuery(PRODUCT_QUERY, {
    variables: {
      where: {
        id: parseInt(product_id),
      },
    },
  });

  // get update user mutation extracted
  const [
    UpdateProduct,
    // { data, loading, error}
  ] = useMutation(UPDATE_PRODUCT_MUTATION, {
    onCompleted: () => {
      console.log(333555, "success");
      showNotifications({ message: "User Updated Succesfully!" });
      // history.push("/users");
    },
    onError: (error) => {
      console.log(333666, error);
      showNotifications({
        type: "error",
        message: "User update failure! Please try again.",
      });
    },
    refetchQueries: [
      //  refetch the user query after success
      {
        query: PRODUCT_QUERY,
        variables: {
          where: {
            id: parseInt(product_id),
          },
        },
      },
    ],
  });

  if (loading) {
    console.log(loading);
    return <div>loading!</div>;
  }
  if (error) {
    console.log(error);
    return <div>Error!</div>;
  }
  if (data) {
    const { product } = data;
    console.log(32323232, product);

    // prepare pictures to match their style
    if (product.image_url) {
      product.image_url = [
        {
          uid: "1",
          name: "image_url",
          status: "done",
          url: product.image_url,
        },
      ];
    }

    // show user to the form as initial data then fire update mutation when needed
    return (
      <div>
        <h1>Update Product</h1>
        <ProductForm
          isUpdate={true}
          initialValues={product}
          updateFunction={UpdateProduct}
        />
      </div>
    );
  }
};

export default withRouter(UpdateProduct);
