import React from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  Table,
  Button,
  Alert,
  // Icon,
  Menu,
  Modal,
} from "antd";
import { withRouter } from "react-router-dom";
import { CREATE_BACKUP_MUTATION } from "../mutations/CreateBackup";
import { RESTORE_BACKUP_MUTATION } from "../mutations/restoreBackup";
import { BACKUPS_QUERY } from "../queries/backups";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

const { confirm } = Modal;

const actionMenu = ({ record, restoreBackup }) => (
  <Menu>
    <Menu.Item key="restore">
      <div
        onClick={() => {
          confirm({
            okText: "Restore",
            okType: "danger",
            cancelText: "Cancel",
            title: "All unbackuped changes will lost.",
            content: "Please make sure you have backuped the system!",
            onOk() {
              return restoreBackup({
                variables: {
                  backup_id: record.name,
                },
              });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; Restore
      </div>
    </Menu.Item>
  </Menu>
);

const columns = (restoreBackup) => [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, restoreBackup })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Setting
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const Backups = ({ match }) => {
  const [
    createBackup,
    // { loading: createBackupLoading, error: createBackupError },
  ] = useMutation(CREATE_BACKUP_MUTATION, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "System Backup Succesful!" });
    },
    onError: () => {
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Backup failed!.",
      });
    },
    refetchQueries: [{ query: BACKUPS_QUERY }],
  });

  const [restoreBackup, { loading: restoreBackupLoading }] = useMutation(
    RESTORE_BACKUP_MUTATION,
    {
      onCompleted: () => {
        showNotifications({ message: "System Restore Succesful!" });
      },
      onError: () => {
        showNotifications({
          type: "error",
          message: "Something went wrong! Restoration failed!.",
        });
      },
      refetchQueries: [{ query: BACKUPS_QUERY }],
    }
  );

  const { loading, error, data } = useQuery(BACKUPS_QUERY);
  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  return (
    <div>
      <TableActions>
        {/* <div className="search-box-wrapper">
          <Search
            value={searchValue}
            className="search-box"
            placeholder="Search a plant"
            onChange={(event) => {
              const { value } = event.target;
              setSearchValue(value);
              filterPlants({ value, refetch });
            }}
          />
        </div> */}

        <Button
          type="primary"
          disabled={restoreBackupLoading}
          // icon="plus"
          onClick={() => {
            createBackup();
            // run backup function here
          }}
        >
          Backup Now
        </Button>
      </TableActions>

      <Table
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns(restoreBackup)}
        dataSource={data.backups}
      />
    </div>
  );
};

export default withRouter(Backups);
