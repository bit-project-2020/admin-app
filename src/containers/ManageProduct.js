// this container is used to manage product related stuff like product parameters etc.
import { Divider } from "antd";

import React from "react";
import { useQuery } from "@apollo/react-hooks";

import { withRouter } from "react-router-dom";

// import childComponents
import SizeManager from "../components/Product/SizeManager";
import DeviationManager from "../components/Product/DeviationManager";
import ProductSpecifications from "../containers/ProductSpecifications";

import { Tabs } from "antd";

import { SIZES_QUERY } from "../queries/sizes";
import { PRODUCT_QUERY } from "../queries/product";

const { TabPane } = Tabs;

const ManageProduct = ({ match }) => {
  // get the product_id to fetch product sizes
  let { product_id } = match.params;
  product_id = parseInt(product_id);

  const {
    loading: loadingProduct,
    error: productQueryError,
    data: productData,
  } = useQuery(PRODUCT_QUERY, {
    variables: {
      where: {
        id: product_id,
      },
    },
  });

  const {
    loading: loadingSizes,
    error: sizesQueryError,
    data: productSizes,
  } = useQuery(SIZES_QUERY, {
    variables: {
      where: {
        product_id: product_id,
      },
    },
  });

  if (loadingProduct || loadingSizes) {
    return <h2> loading...! </h2>;
  }
  if (productQueryError || sizesQueryError) {
    console.log(productQueryError);
    console.log(sizesQueryError);
  }
  if (productData) {
    console.log(300300, productData);
    const { product } = productData;
    return (
      <div>
        <h1> Product: {product.name} </h1>
        <Tabs
          defaultActiveKey={"1"}
          //  onChange={callback}
        >
          <TabPane tab="Manage Sizes" key="1">
            <Divider orientation="left">
              <h1> Sizes/ Variations</h1>
            </Divider>
            <SizeManager id={product_id} />
          </TabPane>
          <TabPane tab="Manage Deviations" key="2">
            <Divider orientation="left">
              <h1>Manage Size related deviations </h1>
            </Divider>
            <DeviationManager product={productData} sizes={productSizes} />
          </TabPane>
          <TabPane tab="Manage Specifications" key="3">
            {/* <Divider orientation="left"></Divider> */}
            <ProductSpecifications
              product_id={product.id}
              plant={product.plant}
            />
          </TabPane>
        </Tabs>
      </div>
    );
  }
};

export default withRouter(ManageProduct);
