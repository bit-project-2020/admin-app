import React from "react";
import { useMutation } from "@apollo/react-hooks";

import { PLANTS_QUERY } from "../queries/plants";
import { CREATE_PLANT_MUTATION } from "../mutations/CreatePlant";

import PlantForm from "../components/forms/plantForm";
import showNotifications from "../lib/showNotifications";

import styled from "styled-components";

const CreatePlantWrapper = styled.div`
  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;

const AddPlant = ({ history }) => {
  const [
    createPlant,
    // { data, loading, error}
  ] = useMutation(CREATE_PLANT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Plant successfully added!" });
      history.push("/plants");
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: PLANTS_QUERY }],
  });

  return (
    <CreatePlantWrapper>
      <div className="form-title">Create a new plant</div>
      <PlantForm
        isUpdate={false}
        createFunction={createPlant}
        initialValues={{
          is_active: true,
        }}
      />
    </CreatePlantWrapper>
  );
};

export default AddPlant;
