import React from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { DELETE_PLANT_PARAMETER } from "../mutations/DeletePlantParameter";
import { PLANT_PARAMETERS_QUERY } from "../queries/plantParameters";
import styled from "styled-components";

import {
  Table,
  Button,
  Alert,
  // Icon,
  Menu,
  Modal,
} from "antd";
import {
  //LoadingOutlined, DownOutlined, UploadOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { Link, withRouter } from "react-router-dom";

// import debounce from "lodash/debounce";

import { UPDATE_PLANT_PARAMETER_MUTATION } from "../mutations/UpdatePlantParameter";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import {
  // EditOutlined,
  ToolOutlined,
  DeleteOutlined,
} from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

const ManagePlantParametersWrapper = styled.div`
  tr.deactivated {
    background-color: #f5f0be99;
  }

  div.column-active {
    color: green;
  }

  div.column-inactive {
    color: red;
  }

  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;

const { confirm } = Modal;

const actionMenu = ({ record, deletePlantParameter, deactivateParameter }) => {
  return (
    <Menu>
      <Menu.Item key="deletePlant">
        <div
          onClick={() => {
            console.log(record);

            confirm({
              okText: "Yes",
              okType: "danger",
              cancelText: "No",
              title: "Do you really want to delete this plant parameter?",
              content:
                "All the data stored under that parameter will be also deleted!",
              onOk() {
                return deletePlantParameter({
                  variables: {
                    where: {
                      id: record.id,
                    },
                  },
                });
              },
              onCancel() {},
            });
          }}
          role="presentation"
        >
          <DeleteOutlined />
          &nbsp; Delete
        </div>
      </Menu.Item>

      <Menu.Item key="deactivate_plant_parameter">
        <div
          onClick={() => {
            confirm({
              okText: "Yes",
              okType: "danger",
              cancelText: "No",
              title: `Do you really want to ${
                record.is_active ? "deactivate" : "reactivate"
              } this plant parameter?`,
              content: "This parameter will not visible on generated forms!",
              onOk() {
                return deactivateParameter({
                  variables: {
                    where: {
                      id: record.id,
                    },
                    data: {
                      is_active: record.is_active === true ? false : true,
                    },
                  },
                });
              },
              onCancel() {},
            });
          }}
          role="presentation"
        >
          <DeleteOutlined />
          &nbsp; {record.is_active ? "Deactivate" : "Reactivate"}
        </div>
      </Menu.Item>
    </Menu>
  );
};

// delete plant should be deactivate

const columns = (deletePlantParameter, deactivateParameter) => [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Parameter",
    dataIndex: "parameter",
    key: "parameter",
  },
  {
    title: "isActive",
    dataIndex: "is_active",
    key: "is_active",
    render: (is_active) => {
      if (is_active) {
        return <div className="column-active"> Active </div>;
      } else {
        return <div className="column-inactive"> Inactive </div>;
      }
    },
  },
  {
    title: "Data type",
    dataIndex: "parameter_type",
    key: "parameter_type",
  },
  {
    title: "Parameter type",
    dataIndex: "type",
    key: "type",
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({
            record,
            deletePlantParameter,
            deactivateParameter,
          })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Settings
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const PlantParameters = ({ match }) => {
  // get the plant_name from the query
  const plantName = match.params.plant_name;

  const [
    deletePlantParameter,
    // { loading: deleteAirlineLoading, error: deleteAirlineError },
  ] = useMutation(DELETE_PLANT_PARAMETER, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "plant parameter successfully removed!" });
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong!",
      });
    },
    refetchQueries: [
      {
        query: PLANT_PARAMETERS_QUERY,
        variables: {
          where: {
            plant: plantName,
          },
        },
      },
    ],
  });

  const [
    deactivateParameter,
    // { loading: deleteAirlineLoading },
  ] = useMutation(UPDATE_PLANT_PARAMETER_MUTATION, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "Parameter succesfully deactivated!" });
    },
    onError: (error) => {
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: PLANT_PARAMETERS_QUERY,
        variables: {
          where: {
            plant: plantName,
          },
        },
      },
    ],
  });

  const {
    loading,
    error,
    data,
    // refetch
  } = useQuery(PLANT_PARAMETERS_QUERY, {
    variables: { where: { plant: plantName } },
  });
  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  console.log(data);

  return (
    <ManagePlantParametersWrapper>
      <TableActions>
        <div className="search-box-wrapper">
          <div className="form-title">
            <b> {plantName}</b> plant
          </div>
        </div>

        <Link to={`/add_plant_parameter/${plantName}`}>
          <Button type="primary" icon={<PlusOutlined />}>
            Add Plant Parameter
          </Button>
        </Link>
      </TableActions>

      <Table
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns(deletePlantParameter, deactivateParameter)}
        dataSource={data.plantParameters}
        // later you can use that to pale the deactivate columns
        rowClassName={(record) => {
          if (!record.is_active) {
            return "deactivated";
          }
        }}
      />
    </ManagePlantParametersWrapper>
  );
};

export default withRouter(PlantParameters);
