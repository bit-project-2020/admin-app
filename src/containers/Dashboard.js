import React from "react";
import styled from "styled-components";
// import welcomeBG from "../assets/images/background.jpg";
import User from "../components/User";
import UserCard from "../components/dashboard/UserCard";

const DashboardWrapper = styled.div`
  div#welcome-header {
    h2 {
      margin-top: 1rem;
      font-weight: bold;
    }
  }
`;

const Dashboard = () => (
  <DashboardWrapper>
    <User>
      {(data) => {
        if (data) {
          const { currentUser } = data;

          // console.log(400200, currentUser);
          return (
            <div>
              <UserCard user={currentUser}></UserCard>
              <div id="welcome-header">
                <h2>Welcome to chandima sweets admin panel</h2>
              </div>
            </div>
          );
        }
      }}
    </User>
  </DashboardWrapper>
);

export default Dashboard;
