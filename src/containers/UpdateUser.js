import React from "react";
import { withRouter } from "react-router-dom";

import USER_QUERY from "../queries/User";
import { UPDATE_USER_MUTATION } from "../mutations/UpdateUser";

import { useMutation, useQuery } from "@apollo/react-hooks";
import UserForm from "../components/forms/userForm";

import showNotifications from "../lib/showNotifications";

const UpdateUser = ({ match }) => {
  const userID = match.params.user_id;

  // get current user
  const { loading, error, data } = useQuery(USER_QUERY, {
    variables: {
      where: {
        id: parseInt(userID),
      },
    },
  });

  // get update user mutation extracted
  const [
    updateUser,
    // { data, loading, error}
  ] = useMutation(UPDATE_USER_MUTATION, {
    onCompleted: () => {
      console.log(333555, "success");
      showNotifications({ message: "User Updated Succesfully!" });
      // history.push("/users");
    },
    onError: (error) => {
      console.log(333666, error);
      showNotifications({
        type: "error",
        message: "User update failure! Please try again.",
      });
    },
    refetchQueries: [
      //  refetch the user query after success
      {
        query: USER_QUERY,
        variables: {
          where: {
            id: parseInt(userID),
          },
        },
      },
    ],
  });

  if (loading) {
    console.log(loading);
    return <div>loading!</div>;
  }
  if (error) {
    console.log(error);
    return <div>Error!</div>;
  }
  if (data) {
    const { user } = data;

    // prepare pictures to match their style
    if (user.profile_photo) {
      user.profile_photo = [
        {
          uid: "1",
          name: "profile_photo",
          status: "done",
          url: user.profile_photo,
        },
      ];
    }

    // show user to the form as initial data then fire update mutation when needed
    return (
      <div>
        <h1>Update User</h1>
        <UserForm
          isUpdate={true}
          initialValues={user}
          updateFunction={updateUser}
        />
      </div>
    );
  }
};

export default withRouter(UpdateUser);
