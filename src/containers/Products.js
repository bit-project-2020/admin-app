import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Table, Button, Alert, Input, Menu, Modal } from "antd";
import { Link } from "react-router-dom";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { EditOutlined, ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

import { DELETE_PRODUCT_MUTATION } from "../mutations/DeleteProduct";
import { PRODUCTS_QUERY } from "../queries/products";
import { UPDATE_PRODUCT_MUTATION } from "../mutations/UpdateProduct";
import User from "../components/User";

import styled from "styled-components";

const ProductsWrapper = styled.div`
  tr.deactivated {
    background-color: #f5f0be99;
  }

  div.column-active {
    color: green;
  }
  div.column-inactive {
    color: red;
  }
`;

const { confirm } = Modal;
const { Search } = Input;

const actionMenu = ({ record, deleteProduct, updateProduct }) => {
  return (
    <Menu>
      <Menu.Item key="updateProduct">
        <Link to={`/products/${record.id}/update`}>
          <EditOutlined />
          &nbsp; Update
        </Link>
      </Menu.Item>

      <Menu.Item key="deactivateProduct">
        <div
          onClick={() => {
            updateProduct({
              variables: {
                where: {
                  id: record.id,
                },
                data: {
                  is_active: !record.is_active,
                },
              },
            });
          }}
        >
          <EditOutlined />
          &nbsp; &nbsp;
          {record.is_active ? " Deactivate" : " Activate"}
        </div>
      </Menu.Item>

      {/* <Menu.Item key="manageSpecs">
        <Link
          to={`/manage_product_specifications/${record.id}/${record.plant}`}
        >
          <EditOutlined />
          &nbsp; Manage Specifications
        </Link>
      </Menu.Item> */}

      <Menu.Item key="ManageProduct">
        <Link to={`/manage_product/${record.id}`}>
          <EditOutlined />
          &nbsp; Manage product
        </Link>
      </Menu.Item>

      <Menu.Item key="deletePlant">
        <div
          onClick={() => {
            confirm({
              okText: "Yes",
              okType: "danger",
              cancelText: "No",
              title: "Do you really want to delete this product?",
              content:
                "All the products that associated with this product will be removed!",
              onOk() {
                return deleteProduct({
                  variables: {
                    where: {
                      id: record.id,
                    },
                  },
                });
              },
              onCancel() {},
            });
          }}
          role="presentation"
        >
          <DeleteOutlined />
          &nbsp; &nbsp;Delete
        </div>
      </Menu.Item>
    </Menu>
  );
};

const columns = (deleteProduct, updateProduct) => [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Plant",
    dataIndex: "plant",
    key: "plant",
  },
  {
    title: "Product Active",
    dataIndex: "is_active",
    key: "is_active",
    render: (is_active) => {
      if (is_active) {
        return <div className="column-active"> Active </div>;
      } else {
        return <div className="column-inactive"> Inactive </div>;
      }
    },
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, deleteProduct, updateProduct })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Setting
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const Products = () => {
  const [searchValue, setSearchValue] = useState("");

  const [
    deleteProduct,
    // { loading, error },
  ] = useMutation(DELETE_PRODUCT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Product successfully added!" });
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: PRODUCTS_QUERY }],
  });

  const [
    updateProduct,
    // { loading, error },
  ] = useMutation(UPDATE_PRODUCT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Product successfully updated!" });
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: PRODUCTS_QUERY }],
  });

  const { loading, error, data } = useQuery(PRODUCTS_QUERY);

  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  if (data) {
    let products = data.products;

    products = products.filter((product) =>
      product.name.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <ProductsWrapper>
        <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search a product"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>

          <User>
            {(data) => {
              const { role } = data.currentUser;
              console.log(role);

              return (
                <Link to="/products/add">
                  <Button
                    type="primary"
                    // icon="plus"
                  >
                    Add Product
                  </Button>
                </Link>
              );
            }}
          </User>
        </TableActions>

        <Table
          rowClassName={(record) => {
            if (!record.is_active) {
              return "deactivated";
            }
          }}
          loading={loading}
          bordered
          rowKey={(record) => record.id}
          columns={columns(deleteProduct, updateProduct)}
          dataSource={products}
        />
      </ProductsWrapper>
    );
  }
};

export default Products;
