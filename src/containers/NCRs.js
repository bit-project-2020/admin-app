import React from "react";
import styled from "styled-components";

import { useQuery } from "@apollo/react-hooks";
import { NCRS_QUERY } from "../queries/ncrs";

import { Table, Button } from "antd";

const columns = (deleteProduct) => [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Created At",
    dataIndex: "created_at",
    key: "created_at",
    render: (record, value) => {
      const now = new Date(record);
      return (
        <div>
          {now.toLocaleDateString()} {now.toLocaleTimeString()}
        </div>
      );
    },
  },
  {
    title: "Plant",
    dataIndex: "plant",
    key: "plant",
  },
  {
    title: "Product",
    dataIndex: "product_name",
    key: "product_name",
  },
  {
    title: "Size",
    dataIndex: "size_name",
    key: "size_name",
  },
  {
    title: "isActive",
    dataIndex: "is_active",
    key: "is_active",
    render: (is_active) => <span>{is_active ? "Yes" : "No"} </span>,
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <Button
          onClick={() => {
            window.open(
              `${
                record.type === "PROCESS" ? "process-data" : "packaging-data"
              }/${record.plant}/${record.record_id}?focusNCR=true`,
              "_blank"
            );
          }}
        >
          view in detail
        </Button>
      );
    },
  },
];

const NCRsWrapper = styled.div``;

const NCRs = () => {
  const { loading, error, data } = useQuery(NCRS_QUERY);

  if (loading) {
    return <div> Loading! </div>;
  }
  if (error) {
    return <div> Error! </div>;
  }
  if (data) {
    console.log(data);
    return (
      <NCRsWrapper>
        <Table
          rowClassName={(record, index) => {
            if (record.is_active === false) {
              return "deviated";
            }
          }}
          loading={loading}
          bordered
          rowKey={(record) => record.id}
          columns={columns()}
          dataSource={data.ncrs}
        />
      </NCRsWrapper>
    );
  }
};

export default NCRs;
