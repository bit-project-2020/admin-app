import React from "react";
import { useQuery } from "@apollo/react-hooks";

import { INSPECTION_RECORD_ONE_QUERY } from "../queries/inspectionRecordOne";

import { withRouter } from "react-router-dom";

import { useLocation } from "react-router-dom";

import InspectionDataContainer from "../components/inspection_data_one/inspection_data_container";
import NCRData from "../components/inspection_data_one/ncr_data";

import { Tabs } from "antd";

const { TabPane } = Tabs;

const InspectionDataOne = ({ history }) => {
  let location = useLocation();

  if (location.search.includes("focusNCR=true")) {
    console.log("lets focus ncr");
  }

  const { pathname } = location;
  const record_type =
    pathname.split("/")[1] === "process-data" ? "PROCESS" : "PACKAGING";
  const plant = pathname.split("/")[2];
  const record_id = parseInt(pathname.split("/")[3]);

  const { loading, error, data, refetch } = useQuery(
    INSPECTION_RECORD_ONE_QUERY,
    {
      variables: {
        record_type,
        plant,
        record_id,
      },
    }
  );

  if (loading) {
    return <div>Loading</div>;
  }
  if (error) {
    return <div> Something went wrong </div>;
  }

  if (data) {
    // if there is no ncr disable the ncr section.
    const { inspectionDataRecord: record } = data;

    return (
      <Tabs
        defaultActiveKey={location.search.includes("focusNCR=true") ? "2" : "1"} // if the focus ncr flag is in the url focus to the NCR section
        //  onChange={callback}
      >
        <TabPane tab="DATA" key="1">
          <InspectionDataContainer
            data={data}
            record_type={record_type}
            plant={plant}
            record_id={record_id}
            refetchTheRecord={refetch}
            pathname={pathname}
          />
        </TabPane>
        <TabPane
          tab="NCR"
          key="2"
          disabled={!record.deviations.length > 0 || record.ncr === null}
        >
          <NCRData
            data={data.inspectionDataRecord}
            refetch={refetch}
            refetchParams={(record_type, record_id, plant)}
          />
        </TabPane>
      </Tabs>
    );
  }
};

export default withRouter(InspectionDataOne);
