import React from "react";

import { withRouter } from "react-router-dom";

import { PLANTS_QUERY } from "../queries/plants";
import { UPDATE_PLANT_MUTATION } from "../mutations/UpdatePlant";

import { useMutation, useQuery } from "@apollo/react-hooks";
import PlantForm from "../components/forms/plantForm";

import styled from "styled-components";

import showNotifications from "../lib/showNotifications";

const UpdatePlantWrapper = styled.div`
  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;

const UpdatePlant = ({ match }) => {
  const { plant } = match.params;

  // get current product
  const { loading, error, data } = useQuery(PLANTS_QUERY, {
    variables: {
      where: {
        plant_name: plant,
      },
    },
  });

  // get update user mutation extracted
  const [
    UpdatePlant,
    // { data, loading, error}
  ] = useMutation(UPDATE_PLANT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "User Updated Succesfully!" });

      // have to refetch plants query
      // history.push("/users");
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "User update failure! Please try again.",
      });
    },
    refetchQueries: [
      //  refetch the user query after success
      {
        query: PLANTS_QUERY,
        variables: {
          where: {
            plant_name: plant,
          },
        },
      },
      {
        query: PLANTS_QUERY,
      },
    ],
  });

  if (loading) {
    console.log(loading);
    return <div>loading!</div>;
  }
  if (error) {
    console.log(error);
    return <div>Error!</div>;
  }
  if (data) {
    console.log(650210, data);
    const { plants } = data;
    const plant = plants[0];

    console.log(600200, plant.image_url);
    // prepare pictures to match their style
    if (plant.image_url) {
      plant.image_url = [
        {
          uid: "1",
          name: "image_url",
          status: "done",
          url: plant.image_url,
        },
      ];
    }

    // show user to the form as initial data then fire update mutation when needed
    return (
      <UpdatePlantWrapper>
        <div className="form-title">
          Update Plant, <b>{plant.plant_name}</b>
        </div>
        <PlantForm
          isUpdate={true}
          initialValues={plant}
          updateFunction={UpdatePlant}
        />
      </UpdatePlantWrapper>
    );
  }
};

export default withRouter(UpdatePlant);
