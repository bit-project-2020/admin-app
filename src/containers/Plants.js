import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import styled from "styled-components";
import {
  Table,
  Button,
  Alert,
  Input,
  // Icon,
  Menu,
  Modal,
} from "antd";
import { Link, withRouter } from "react-router-dom";
import { DELETE_PLANT_MUTATION } from "../mutations/DeletePlant";
import { PLANTS_QUERY } from "../queries/plants";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { EditOutlined, ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

const PlantWrapper = styled.div`
  tr.deactivated {
    background-color: #f5f0be99;
  }

  div.column-active {
    color: green;
  }
  div.column-inactive {
    color: red;
  }
`;

const { confirm } = Modal;
const { Search } = Input;

const actionMenu = ({ record, deletePlant }) => (
  <Menu>
    <Menu.Item key="updatePlant">
      <Link to={`/update_plants/${record.plant_name}`}>
        <EditOutlined />
        &nbsp; Update
      </Link>
    </Menu.Item>

    <Menu.Item key="managePlantParameters">
      <Link to={`/manage_plant_parameters/${record.plant_name}`}>
        <EditOutlined />
        &nbsp; Manage Plant Parameters
      </Link>
    </Menu.Item>

    <Menu.Item key="deletePlant">
      <div
        onClick={() => {
          confirm({
            okText: "Yes",
            okType: "danger",
            cancelText: "No",
            title: "Do you really want to delete this plant?",
            content:
              "All the stuff belonging to this plant will be removed forever!",
            onOk() {
              return deletePlant({
                variables: {
                  plant_name: record.plant_name,
                },
              });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; Delete
      </div>
    </Menu.Item>
  </Menu>
);

const columns = (deletePlant) => [
  {
    title: "Name",
    dataIndex: "plant_name",
    key: "plant_name",
  },
  {
    title: "isActive",
    dataIndex: "is_active",
    key: "is_active",
    render: (is_active) => {
      if (is_active) {
        return <div className="column-active"> Active </div>;
      } else {
        return <div className="column-inactive"> Inactive </div>;
      }
    },
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, deletePlant })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Setting
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const Plants = ({ match }) => {
  const [searchValue, setSearchValue] = useState("");

  const [
    deletePlant,
    // { loading: deleteAirlineLoading, error: deleteAirlineError },
  ] = useMutation(DELETE_PLANT_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Plant successfully deleted!" });
    },
    onError: (error) => {
      let message = "Something went wrong! Please try again.";
      if (error.message.includes("plant_parameters_fk")) {
        message = "Unable to delete! Plant has associated with parameters";
      } else if (error.message.includes("product_fk")) {
        message = "Unable to delete! Plant has products";
      }
      showNotifications({
        type: "error",
        message,
      });
    },
    refetchQueries: [{ query: PLANTS_QUERY }],
  });

  const { loading, error, data } = useQuery(PLANTS_QUERY);
  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  if (data) {
    // manage data here

    let plants = data.plants;
    // filtered data
    plants = plants.filter((plant) =>
      plant.plant_name.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <PlantWrapper>
        <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search by plant name"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>

          <Link to="/plants/add">
            <Button
              type="primary"
              // icon="plus"
            >
              Add Plant
            </Button>
          </Link>
        </TableActions>

        <Table
          rowClassName={(record) => {
            if (!record.is_active) {
              return "deactivated";
            }
          }}
          loading={loading}
          bordered
          rowKey={(record) => record.id}
          columns={columns(deletePlant)}
          dataSource={plants}
        />
      </PlantWrapper>
    );
  }
};

export default withRouter(Plants);
