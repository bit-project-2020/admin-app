import React, { useState, useEffect } from "react";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";

import { INSPECTION_DATA_QUERY } from "../queries/inspectionData";

import { PLANTS_QUERY } from "../queries/plants";
import { SIZES_QUERY } from "../queries/sizes";
import { PRODUCTS_QUERY } from "../queries/products";
import styled from "styled-components";

import showNotifications from "../lib/showNotifications";

import moment from "moment";

import {
  useLocation,
  //useHistory
} from "react-router-dom";

import { Table, Space, Select, Button, Form, DatePicker } from "antd";

const InspectionDataWrapper = styled.div`
  .highlight-in-red {
    color: red;
  }
  .ant-select {
    min-width: 100px;
  }
`;

const { Option } = Select;

const columns = ({ structure, plant, pathname }) => {
  const extra_fields = (structure && structure.extra_fields) || [];

  const fields = [];

  extra_fields.forEach((field) => {
    if (field.type === "IMAGE") {
      return;
    }
    fields.push({
      title: field.parameter,
      dataIndex: field.parameter,
      key: field.parameter,
      render: (text, record) => {
        const relaventRecord = record.extra_fields.find(
          (extra_field) => extra_field.parameter === field.parameter
        );

        if (relaventRecord.type === "BOOLEAN") {
          return <>{relaventRecord.boolean_value ? "ok" : "not_ok"}</>;
        }
        if (relaventRecord.type === "NUMBER" || field.type === "NUMBER_EXACT") {
          return <>{relaventRecord.numeric_value}</>;
        }
        if (relaventRecord.type === "DATE") {
          console.log(relaventRecord);
          const date = new Date(parseInt(relaventRecord.text_value));
          return <>{date.toLocaleDateString()}</>;
        }
        if (relaventRecord.type === "TEXT") {
          console.log(relaventRecord);
          return <>{relaventRecord.text_value}</>;
        }
      },
    });
  });

  return [
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
      render(text, record) {
        const now = new Date(text);
        return (
          <>
            {now.toLocaleDateString()} {now.toLocaleTimeString()}
          </>
        );
      },
    },
    {
      title: "Shift",
      dataIndex: "shift",
      key: "shift",
    },
    ...fields,
    {
      title: "Comment",
      dataIndex: "comment",
      key: "comment",
    },
    // handle actions field here
    {
      title: "Actions",
      dataIndex: "actions",
      key: "actions",
      align: "center",
      render(text, record) {
        return (
          <Button
            onClick={() => {
              //history.push("/inspectionData");

              // window.open function returns an object you can use its focus() method like below
              // win.focus();
              window.open(`${pathname}/${plant}/${record.id}`, "_blank");
            }}
          >
            view in detail
          </Button>
        );
      },
    },
  ];
};

const InspectionData = () => {
  let location = useLocation();
  const today = new Date();

  let sevenDaysBefore = new Date();
  sevenDaysBefore.setDate(sevenDaysBefore.getDate() - 7);

  const [startDate, setStartDate] = useState(moment(sevenDaysBefore));
  const [endDate, setEndDate] = useState(moment(today));

  // let history = useHistory();
  const { pathname } = location;

  const [plant, setPlant] = useState();
  const [product, setProduct] = useState();
  const [size, setSize] = useState();

  const [fetchInspectionData, { loading, error, data }] = useLazyQuery(
    INSPECTION_DATA_QUERY,
    {
      variables: {
        where: {
          plant: plant,
          type: pathname === "/process-data" ? "PROCESS" : "PACKAGING",
          product: 12,
          size: 11,
        },
      },
    }
  );

  const {
    loading: plantsLoading,
    error: plantsError,
    data: plantsData,
  } = useQuery(PLANTS_QUERY, {});

  const [
    fetchSizesQuery,
    { loading: loadingSizes, data: sizesData, error: sizesError },
  ] = useLazyQuery(SIZES_QUERY);

  const [
    fetchProductsQuery,
    { loading: loadingProducts, data: productsData, error: productsError },
  ] = useLazyQuery(PRODUCTS_QUERY);

  useEffect(() => {
    // handle plant change
    if (!plantsData) {
      return;
    }
    if (plantsData.plants) {
      setPlant(plantsData.plants && plantsData.plants[0].plant_name);
      setProduct(null);
      setSize(null);
    }
  }, [plantsData]);

  useEffect(() => {
    // handle product change
    if (!fetchProductsQuery) {
      return;
    }

    if (plant) {
      fetchProductsQuery({
        variables: {
          where: {
            plant: plant,
          },
        },
      });
    }

    // set newly fetched product to the state
    if (productsData && productsData.products) {
      if (productsData.products[0]) {
        setProduct(productsData.products[0].id);
      } else {
        setProduct(null);
      }
      console.log("setting the product");
    }
  }, [fetchProductsQuery, plant, plantsData, productsData]);

  useEffect(() => {
    // handle size change
    if (!fetchSizesQuery) {
      return;
    }

    if (product) {
      console.log(350350, product);
      fetchSizesQuery({
        variables: {
          where: {
            product_id: product,
          },
        },
      });
    } else {
      // set size to null if there is no product selected
      setSize(null);
      return;
    }

    if (sizesData && sizesData.sizes) {
      if (sizesData.sizes[0]) {
        setSize(sizesData.sizes[0].id);
      } else {
        setSize(null);
      }
      console.log(408172, sizesData);
    }
  }, [fetchSizesQuery, sizesData, product]);

  // if there is a size fetch the data
  useEffect(() => {
    if (product === null || size === null) {
      return;
    }

    if (!fetchInspectionData) {
      return;
    }

    if (size) {
      fetchInspectionData({
        variables: {
          where: {
            plant: plant,
            type: pathname === "/process-data" ? "PROCESS" : "PACKAGING",
            product: product,
            size: size,
            date_gt: startDate.startOf("day").format(),
            date_lt: endDate.endOf("day").format(),
          },
        },
      });
    }
  }, [size, product, fetchInspectionData, pathname, plant, startDate, endDate]);

  if (loading || plantsLoading || loadingProducts || loadingSizes) {
    return <div>Loading</div>;
  }
  if (error || plantsError || productsError || sizesError) {
    return <div> Something went wrong </div>;
  }

  if (!plantsData) {
    return <div> no plants were found </div>;
  }

  if (!productsData) {
    return <div> no products were found</div>;
  }

  if (!sizesData) {
    return <div> No Sizes are created yet for the selected size </div>;
  }

  if (!data) {
    return <div> No data received </div>;
  }

  const { inspectionData } = data;
  const { plants } = plantsData;
  const { products } = productsData;
  const { sizes } = sizesData;

  return (
    <InspectionDataWrapper>
      <h1> Filtering options </h1> <br />
      <Space direction={"horizontal"}>
        {/* for plant */}
        <Form.Item
          label="Select Plant"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Select
            mode="default"
            disabled={plantsLoading}
            name="plant"
            defaultValue={plant}
            value={plant}
            // showSearch
            // defaultActiveFirstOption={false}
            showArrow
            placeholder="plant"
            filterOption={false}
            // onSearch={searchField.search}
            onSelect={(value) => {
              console.log(320320, value);
              setPlant(value);
            }}
          >
            {plants.map((plant) => {
              return (
                <Option value={plant.plant_name} key={plant.plant_name}>
                  {plant.plant_name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>

        {/* for product */}
        <Form.Item
          label="Select Product"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Select
            mode="default"
            disabled={loadingProducts}
            name="Product"
            defaultValue={product}
            value={product}
            // showSearch
            // defaultActiveFirstOption={false}

            showArrow
            placeholder="Product"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
            onSelect={(value) => {
              setProduct(value);
            }}
          >
            {products.map((product) => {
              return (
                <Option value={product.id} key={product.id}>
                  {product.name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        {/* for size */}

        <Form.Item
          label="Select Size"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Select
            mode="default"
            disabled={loadingSizes}
            name="size"
            defaultValue={size}
            value={size}
            // showSearch
            // defaultActiveFirstOption={false}
            showArrow
            placeholder="Size"
            filterOption={false}
            // onSearch={searchField.search}
            // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
            onSelect={(value) => {
              setSize(value);
            }}
          >
            {sizes.map((size) => {
              return (
                <Option value={size.id} key={size.id}>
                  {size.size_name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
      </Space>
      <br />
      <Space direction={"horizontal"}>
        <Form.Item label="Start Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (endDate.diff(date, "days") < 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setStartDate(date);
              }
            }}
            value={startDate}
          />
        </Form.Item>

        {/* for product */}
        <Form.Item label="End Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (startDate.diff(date, "days") > 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setEndDate(date);
              }
            }}
            value={endDate}
          />
        </Form.Item>
      </Space>
      <br />
      {(productsData && productsData.products.length === 0 && (
        <div className="highlight-in-red">This plant has no products yet</div>
      )) || <div>&nbsp;</div>}
      {/* empty code will take an empty line to reserve that space or print the statement. anyway line cost is one */}
      {sizesData && sizesData.sizes.length === 0 && (
        <div className="highlight-in-red">This product has no sizes yet</div>
      )}
      <Table
        rowClassName={(record, index) => {
          if (record.is_deviated === true) {
            return "deviated";
          }
        }}
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns({ structure: inspectionData[0], plant, pathname })}
        dataSource={inspectionData}
      />
    </InspectionDataWrapper>
  );
};

export default InspectionData;
