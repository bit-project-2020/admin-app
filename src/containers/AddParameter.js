import React from "react";
import { useMutation } from "@apollo/react-hooks";
import { Formik } from "formik";

import * as Yup from "yup";

import styled from "styled-components";

import { Input, Select, Form, Button } from "antd";

import { CREATE_SPEC_PARAMETER_MUTATION } from "../mutations/CreateParameter";

import { PARAMETERS_QUERY } from "../queries/Parameters";

import showNotifications from "../lib/showNotifications";

const AddParameterWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }

  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;
// import searchFieldDataExtractor from "../lib/searchFieldDataExtractor";

const { Option } = Select;

const CreateParameterSchema = Yup.object().shape({
  name: Yup.string()
    .min(1, "Too Short!")
    .max(17, "Too Long parameter name!")
    .test(
      "contain-invalid-characters",
      "Spaces and '.' characters are not allowed",
      (value) => {
        if (value !== undefined && value !== null) {
          if (typeof value === "string") {
            return !(value.includes(" ") || value.includes("."));
          }
        } else {
          return true;
        }
      }
    )
    .test("lowercase-only", "Only lowercase characters allowed", (value) => {
      if (value !== undefined && value !== null) {
        if (value.toLowerCase() !== value) {
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    })
    .required("Parameter is a required field"),
  parameter_type: Yup.string(),
  type: Yup.string().required("Required"),
});

// additionally filter the parameter not to have a reserved postgresql words

const AddParameter = ({ history }) => {
  const [
    createSpecParameter,
    // { data, loading, error }
  ] = useMutation(CREATE_SPEC_PARAMETER_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Parameter successfully added!" });
      history.push("/parameters");
    },
    onError: (error) => {
      console.log(error);

      let message = "Something went wrong! Please try again.";
      if (error.message.includes("duplicate key value")) {
        message = "Parameter value alreay exists";
      }

      showNotifications({
        type: "error",
        message,
      });
    },
    refetchQueries: [{ query: PARAMETERS_QUERY }],
  });

  return (
    <AddParameterWrapper>
      <div className="form-title">Add a new parameter</div>
      <Formik
        // set initial values to the table
        validationSchema={CreateParameterSchema}
        initialValues={{
          name: "",
          parameter_type: "TEXT",
          type: "PACKAGING",
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          createSpecParameter({
            variables: {
              data: {
                ...values,
              },
            },
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <Form.Item
              label="Parameter"
              validateStatus={
                errors.name && touched["name"] ? "error" : "validating"
              }
              // help={errors.name}
            >
              <Input
                placeholder="Paramter name"
                onChange={handleChange}
                onBlur={handleBlur}
                name="name"
                value={values.name}
              />
              {errors.name && touched.name && errors.name}
            </Form.Item>

            <Form.Item label="Data Type">
              <Select
                defaultValue="TEXT"
                style={{ width: 120 }}
                onChange={(value) => {
                  console.log(value);
                  values.parameter_type = value;
                }}
                name="parameter_type"
              >
                <Option value="TEXT">TEXT</Option>
                <Option value="NUMBER">NUMBER</Option>
                <Option value="NUMBER_EXACT">NUMBER_EXACT</Option>
                <Option value="DATE">DATE</Option>
                <Option value="BOOLEAN">BOOLEAN</Option>
                <Option value="IMAGE">IMAGE</Option>
              </Select>
            </Form.Item>

            <Form.Item label="parameter mode">
              <Select
                defaultValue="PACKAGING"
                style={{ width: 120 }}
                onChange={(value) => {
                  console.log(value);
                  values.type = value;
                }}
                name="type"
              >
                <Option value="PACKAGING">PACKAGING</Option>
                <Option value="PROCESS">PROCESS</Option>
              </Select>
              {errors.type && touched.type && errors.type}
            </Form.Item>

            <Form.Item
              wrapperCol={{
                xs: { span: 24, offset: 0 },
                sm: { span: 6, offset: 3 },
                xl: { span: 6, offset: 2 },
              }}
            >
              <Button
                type="primary"
                htmlType="submit"
                disabled={isSubmitting}
                className="session-form-button"
                loading={isSubmitting}
                //icon={<PlusOutlined />}
              >
                Create Parameter
              </Button>
            </Form.Item>
          </form>
        )}
      </Formik>
    </AddParameterWrapper>
  );
};

export default AddParameter;
