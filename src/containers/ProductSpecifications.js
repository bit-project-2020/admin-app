import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Modal } from "antd";

import styled from "styled-components";

import SpecRenderer from "../components/SpecHandlers/SpecRenderer";
import { Radio, Space } from "antd";

import { SIZES_QUERY } from "../queries/sizes";
import { SPECIFICATIONS_QUERY } from "../queries/specifications";
import { PRODUCT_QUERY } from "../queries/product";
import { DEVIATIONS_QUERY } from "../queries/deviations";
import { PLANT_PARAMETERS_QUERY } from "../queries/plantParameters";
import { CREATE_SPECIFICATION } from "../mutations/CreateSpecification";
import { UPDATE_SPECIFICATION_MUTATION } from "../mutations/UpdateSpecification";

import showNotifications from "../lib/showNotifications";
import SpecificationForm from "../components/forms/specificationForm";

const ProductSpecificationsWrapper = styled.div`
  #page-topic {
    font-size: 1.5rem;
  }

  div.ant-modal-header {
    div.ant-modal-title {
      font-size: 1.5rem !important;
    }
  }
`;

const ProductSpecifications = ({ product_id, plant }) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [itemToChange, setItemToChange] = useState({});
  const [selectedType, setSelectedType] = useState("PROCESS");

  const [createSpecification] = useMutation(CREATE_SPECIFICATION, {
    onCompleted: () => {
      console.log(3003000, "model closing fired");
      setModalOpen(false);
      showNotifications({ message: "Specification successfully created!" });
    },
    onError: () => {
      setModalOpen(false);
      setItemToChange({});
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: SPECIFICATIONS_QUERY,
        variables: {
          product_id: parseInt(product_id),
        },
      },
    ],
  });

  const [updateSpecification] = useMutation(UPDATE_SPECIFICATION_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Specification successfully updated!" });
      setModalOpen(false);
    },
    onError: () => {
      setModalOpen(false);
      setItemToChange({});
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: SPECIFICATIONS_QUERY,
        variables: {
          product_id: parseInt(product_id),
        },
      },
    ],
  });

  const {
    loading: specificationsLoading,
    error: specificationsError,
    data: specificationsData,
  } = useQuery(SPECIFICATIONS_QUERY, {
    variables: {
      product_id: parseInt(product_id),
    },
  });

  const {
    loading: plantParametersLoading,
    error: plantParametersError,
    data: plantParametersData,
  } = useQuery(PLANT_PARAMETERS_QUERY, {
    variables: {
      where: {
        plant: plant,
      },
    },
  });

  const {
    loading: deviationsLoading,
    error: deviationsError,
    data: deviationsData,
  } = useQuery(DEVIATIONS_QUERY, {
    variables: {
      product_id: parseInt(product_id),
    },
  });

  const {
    loading: productLoading,
    error: productError,
    data: productData,
  } = useQuery(PRODUCT_QUERY, {
    variables: {
      where: {
        id: parseInt(product_id),
      },
    },
  });

  const {
    loading: sizesLoading,
    error: sizesError,
    data: sizesData,
  } = useQuery(SIZES_QUERY, {
    variables: {
      where: {
        product_id: parseInt(product_id),
      },
    },
  });

  if (
    specificationsLoading ||
    productLoading ||
    sizesLoading ||
    deviationsLoading ||
    plantParametersLoading
  ) {
    return <div>Loading</div>;
  }
  if (
    specificationsError ||
    productError ||
    sizesError ||
    deviationsError ||
    plantParametersError
  ) {
    // console.log(specificationsError, productError, sizesError, deviationsError);
    return <h1> Error....! </h1>;
  }

  if (
    specificationsData &&
    productData &&
    sizesData &&
    deviationsData &&
    plantParametersData
  ) {
    // console.log(30040000, plantParametersData);
    const { product } = productData;
    const { specifications } = specificationsData;
    let { plantParameters } = plantParametersData;
    const { deviations } = deviationsData;
    const { sizes: productSizes } = sizesData;

    // filter plant_parameters and get only the numeric and boolean parameters
    plantParameters = plantParameters.filter((plantParameter) => {
      return (
        ["NUMBER", "NUMBER_EXACT", "BOOLEAN"].includes(
          plantParameter.parameter_type
        ) && plantParameter.type === selectedType
      );
    });

    // get active deviations
    const activeDeviations = deviations.filter(
      (deviation) => deviation.is_active === true
    );

    // TODO select only sizes with at least one deviation
    const sizes = [];
    activeDeviations.forEach((deviation) => {
      if (!sizes.includes(deviation.size)) {
        sizes.push(deviation.size);
      }
    });

    // then there pass filtered parameters w
    // create an empty specmatrix object to hold specifications
    const specMatrix = {
      default: [],
    };

    productSizes.forEach((size) => {
      specMatrix[size.id] = [];
    });

    // populate the spec matrix
    specifications.forEach((spec) => {
      if (spec.is_default) {
        specMatrix["default"].push(spec);
      } else {
        specMatrix[spec.size].push(spec);
      }
    });

    return (
      <ProductSpecificationsWrapper>
        <h1>
          <Space>
            Specification type:
            <span>
              <Radio.Group
                onChange={(event) => {
                  console.log(event);
                  setSelectedType(event.target.value);
                }}
                value={selectedType}
              >
                <Radio value="PROCESS">PROCESS</Radio>
                <Radio value="PACKAGING">PACKAGING</Radio>
              </Radio.Group>
            </span>
          </Space>
        </h1>
        <SpecRenderer
          specs={specMatrix["default"]}
          plantParameters={plantParameters}
          setModalOpen={setModalOpen}
          setItemToChange={setItemToChange}
          sectionTitle={"General"}
          updateFunction={updateSpecification} // this will be used to activate deactivate
        ></SpecRenderer>
        {/* specifications for size deviated parameters will handle here */}
        {sizes.map((size) => {
          // find the size to tell the
          const masterSizeObj = productSizes.find((item) => item.id === size);

          // filter deviations related to that size
          const sizeRelatedDeviations = activeDeviations.filter(
            (deviation) => deviation.size === size
          );

          // filter parameters related to that size
          const sizeRelatedParams = [];
          sizeRelatedDeviations.forEach((deviation) => {
            const selectedParam = plantParameters.find(
              (param) => param.id === deviation.plant_parameter
            );

            if (selectedParam) {
              sizeRelatedParams.push(selectedParam);
            }
            // console.log(465465, selectedParam);
          });

          return (
            <SpecRenderer
              key={size}
              specs={specMatrix[size]}
              plantParameters={sizeRelatedParams}
              setModalOpen={setModalOpen}
              setItemToChange={setItemToChange}
              sectionTitle={masterSizeObj.size_name}
              size={size}
              updateFunction={updateSpecification} // this will be used to activate deactivate
            />
          );
        })}
        <Modal
          title={`${
            itemToChange.isUpdate === true ? "Update" : "Create"
          } specification`}
          visible={isModalOpen}
          onCancel={() => {
            setModalOpen(false);
            setItemToChange({});
          }}
          footer={null}
        >
          <SpecificationForm
            isUpdate={itemToChange.isUpdate === true}
            product={product}
            initialValues={itemToChange}
            createFunction={createSpecification}
            updateFunction={updateSpecification}
          />
        </Modal>
      </ProductSpecificationsWrapper>
    );
  }
};

export default ProductSpecifications;
