import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { CREATE_PLANT_PARAMETER_MUTATION } from "../mutations/CreatePlantParameter";
import { Formik } from "formik";
import { PLANT_PARAMETERS_QUERY } from "../queries/plantParameters";
import styled from "styled-components";

//TODO correct the path

import {
  Button,
  //Checkbox,
  Select,
  Form,
  Checkbox,
} from "antd";

import { withRouter } from "react-router-dom";

import { PARAMETERS_QUERY } from "../queries/Parameters";
import { UNITS_QUERY } from "../queries/units";

import showNotifications from "../lib/showNotifications";

const AddPlantParameterWrapper = styled.div`
  .ant-col.ant-form-item-label {
    min-width: 150px;
    font-weight: bold;
  }

  .ant-col.ant-form-item-control {
    max-width: 250px;
  }
`;

const { Option } = Select;

const AddPlantParameter = ({ history, match }) => {
  const [unitDisabled, setUnitDisabled] = useState(false);
  const plantName = match.params.plant_name;

  const {
    loading: specParametersLoading,
    error: specParametersError,
    data: specParametersData,
  } = useQuery(PARAMETERS_QUERY);

  const {
    loading: plantParametersLoading,
    error: plantParametersError,
    data: plantParametersData,
  } = useQuery(PLANT_PARAMETERS_QUERY, {
    variables: {
      where: {
        plant: plantName,
      },
    },
  });

  const {
    loading: unitsQueryLoading,
    error: unitsQueryError,
    data: unitsQueryData,
  } = useQuery(UNITS_QUERY);

  // TODO stop letting the users adding the same parameter twice by setting a plant and param unique key

  const [
    createPlantParameter,
    // { data, loading, error }
  ] = useMutation(CREATE_PLANT_PARAMETER_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Plant Parameter successfully added!" });
      history.push(`/manage_plant_parameters/${plantName}`);
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [
      {
        query: PLANT_PARAMETERS_QUERY,
        variables: {
          where: {
            plant: plantName,
          },
        },
      },
    ],
  });

  if (specParametersLoading || unitsQueryLoading || plantParametersLoading) {
    return <div> loading!</div>;
  }

  if (specParametersError || unitsQueryError || plantParametersError) {
    return <div> Error!</div>;
  }

  if (specParametersData && unitsQueryData && plantParametersData) {
    // fileter spec parameters and select which are not associated with the plant
    const plantParameters = plantParametersData.plantParameters;
    const specParameters = specParametersData.specParameters;
    const alreadySelectedParameters = plantParameters.map(
      (plantParam) => plantParam.parameter
    );
    const filterdSpecParameters = specParameters.filter((specParameter) => {
      return (
        !alreadySelectedParameters.includes(specParameter.name) &&
        // here we will throw away is_active false params
        !specParameter.is_active === false
      );
    });

    console.log(2222222, filterdSpecParameters);

    // Unit is hidden for following parameters
    const unitHiddenFor = ["BOOLEAN", "IMAGE", "DATE", "TEXT"];

    return (
      <AddPlantParameterWrapper>
        <h2> Add new parameter, {plantName} Plant</h2>
        <Formik
          // set initial values to the table
          initialValues={{
            plant: plantName,
            parameter: specParametersData.specParameters[0].name,
            unit: unitsQueryData.units[0].unit,
            is_active: true,
          }}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(false);
            createPlantParameter({
              variables: {
                data: {
                  ...values,
                },
              },
            });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <Form.Item label="parameter">
                <Select
                  showSearch
                  style={{ width: 200 }}
                  name="parameter"
                  defaultValue={filterdSpecParameters[0].name}
                  placeholder="Select a parameter"
                  optionFilterProp="children"
                  onChange={(value) => {
                    // here do the other stuff

                    // you need to get the type of the parameter
                    const selectedParam = specParameters.find((parameter) => {
                      return parameter.name === value;
                    });
                    if (unitHiddenFor.includes(selectedParam.parameter_type)) {
                      setFieldValue("unit", "N/A");
                      setUnitDisabled(true);
                    } else {
                      setUnitDisabled(false);
                    }

                    setFieldValue("parameter", value);

                    console.log(300200, selectedParam);
                  }}
                  // onFocus={onFocus}
                  // onBlur={onBlur}
                  // onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {filterdSpecParameters.map((param) => {
                    return (
                      <Option key={param.name} value={param.name}>
                        {param.name} ({param.type})
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>

              <Form.Item label="unit">
                <Select
                  disabled={unitDisabled}
                  showSearch
                  style={{ width: 200 }}
                  name="unit"
                  defaultValue={"N/A -not applicable"}
                  placeholder="Select a Unit"
                  optionFilterProp="children"
                  onChange={(value) => {
                    values.unit = value;
                  }}
                  // onFocus={onFocus}
                  // onBlur={onBlur}
                  // onSearch={onSearch}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {unitsQueryData.units.map((unit) => (
                    <Option key={unit.unit} value={unit.unit}>
                      {unit.unit} - {unit.description}
                    </Option>
                  ))}
                </Select>
              </Form.Item>

              <Form.Item label="isActive">
                <Checkbox
                  onChange={handleChange}
                  name="is_active"
                  checked={values.is_active}
                  // TODO this checkbox should automatically checked
                >
                  Checkbox
                </Checkbox>
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  xs: { span: 24, offset: 0 },
                  sm: { span: 6, offset: 3 },
                  xl: { span: 6, offset: 2 },
                }}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                  //icon={<PlusOutlined />}
                >
                  Add Parameter
                </Button>
              </Form.Item>
              {/* <button type="submit" disabled={isSubmitting}>



                Submit
              </button> */}
            </form>
          )}
        </Formik>
      </AddPlantParameterWrapper>
    );
  }
};

export default withRouter(AddPlantParameter);
