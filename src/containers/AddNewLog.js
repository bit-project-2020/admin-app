import React from "react";
import { DefaultEditor } from "react-simple-wysiwyg";

function AddNewLog() {
  const [html, setHtml] = React.useState("<b>Enter the log here</b>");

  function onChange(e) {
    setHtml(e.target.value);
    console.log(e.target.value);
  }

  return <DefaultEditor value={html} onChange={onChange} />;
}

export default AddNewLog;
