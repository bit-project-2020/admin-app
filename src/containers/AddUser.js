import React from "react";
import { useMutation } from "@apollo/react-hooks";

import { USERS_QUERY } from "../queries/users";

import showNotifications from "../lib/showNotifications";

import UserForm from "../components/forms/userForm";
import styled from "styled-components";

import { CREATE_USER_MUTATION } from "../mutations/CreateUser";

const AddUserWrapper = styled.div`
  .form-title {
    font-size: 1.5rem;
    margin-bottom: 2rem;
    margin-left: 2rem;
  }
`;

const AddUser = ({ history }) => {
  const [
    addUser,
    // { data, loading, error}
  ] = useMutation(CREATE_USER_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "User Created Succesfully!" });
      history.push("/users");
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: USERS_QUERY }],
  });

  return (
    <AddUserWrapper>
      <div className="form-title">Add a new user</div>
      <UserForm isUpdate={false} createFunction={addUser} />
    </AddUserWrapper>
  );
};

export default AddUser;
