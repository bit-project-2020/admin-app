/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import { Layout, Menu, Dropdown, Avatar } from "antd";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import { LogoutOutlined, CaretDownOutlined } from "@ant-design/icons";
import { CURRENT_USER_QUERY } from "../queries/currentUser";
import { LOGOUT_MUTATION } from "../mutations/Logout";
import User from "../components/User";
import capitalizeInitial from "../lib/capitalizeInitial";

import {
  useMutation,
  // useQuery
} from "@apollo/react-hooks";

import Sidebar from "../components/Sidebar";
import showNotifications from "../lib/showNotifications";

const { Header, Content, Footer } = Layout;

const AppTitle = styled.h2`
  margin-left: 16px;
  display: inline-block;
`;

const UserMenu = styled(Dropdown)`
  display: flex;
  align-items: center;
  margin-right: 16px;
  cursor: pointer;

  .username-avatar {
    margin-right: 6px;
    margin-left: 1rem;
  }

  span.usermenu-text {
    display: flex;
  }
`;

const AppContent = styled.div`
  padding: 24px;
  background: #fff;
  min-height: 345px;
`;

export const pageNames = {
  "": "Chandima Sweets Admin Dashboard",
  manage_plant_parameters: "Manage Plant Parameters",
  add_plant_parameter: "Add a Plant Parameter",
  manage_product: "Manage Product",
  update_plants: "Update Plant",
  plants: "Plants",
  parameters: "Parameters",
  data: "Data",
  specifications: "Specifications",
  "process-data": "Process Data",
  "packaging-data": "Packaging Data",
  backup: "Backup",
  logs: "QA Logs",
  ncr: "NCR",
  reports: "Reports",
  products: "Products",
  users: "Users",
  manage_product_specifications: "Manage Product Specifications",
};

const AppLayout = (props) => {
  const today = new Date();
  const [collapsed, setCollapsed] = useState(false);
  const { children, location } = props;
  const pathname = location.pathname.split("/")[1];

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };

  const [
    logout,
    // { loading: deleteAirlineLoading, error: deleteAirlineError },
  ] = useMutation(LOGOUT_MUTATION, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "Logout Succesful!" });
    },
    onError: () => {
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sidebar collapsed={collapsed} onCollapse={onCollapse} />
      <Layout>
        <Header
          style={{
            background: "#fff",
            padding: 0,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <AppTitle>{pageNames[pathname]}</AppTitle>
          {/* <AppTitle>
            <Breadcrumb>
              <Breadcrumb.Item>Reports</Breadcrumb.Item>
              <Breadcrumb.Item>
                <a href=""> Weekly NCR count</a>
              </Breadcrumb.Item>
            </Breadcrumb>
          </AppTitle> */}
          {/* temporally showing to take photographs */}

          <User>
            {(data) => {
              const { currentUser } = data;
              return (
                <UserMenu
                  overlay={
                    <Menu>
                      <Menu.Item
                        key="logout"
                        onClick={() => {
                          logout();
                        }}
                      >
                        <LogoutOutlined /> Sign Out
                      </Menu.Item>
                    </Menu>
                  }
                >
                  <span className="usermenu-text">
                    <div>
                      {capitalizeInitial(currentUser.first_name)} (
                      {currentUser.role})
                    </div>
                    <Avatar
                      shape="square"
                      size={24}
                      className="username-avatar"
                      src={currentUser.profile_photo}
                    />
                    <CaretDownOutlined />
                  </span>
                </UserMenu>
              );
            }}
          </User>
        </Header>
        <Content style={{ margin: "16px" }}>
          <AppContent>{children}</AppContent>
        </Content>

        <Footer style={{ textAlign: "center" }}>
          Chandima Sweets © {today.getUTCFullYear()}
        </Footer>
      </Layout>
    </Layout>
  );
};

// class AppLayout extends Component {

//   onCollapse = collapsed => {
//     this.setState({ collapsed });
//   };

//   render() {
//   }
// }

export default withRouter(AppLayout);
