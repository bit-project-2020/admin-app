/* eslint-disable no-unused-vars */
import React from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  Table,
  Button,
  Alert,
  Input,
  // Icon,
  Menu,
  Modal,
} from "antd";
import { Link, withRouter } from "react-router-dom";
import { USERS_QUERY } from "../queries/users";
import { UPDATE_USER_MUTATION } from "../mutations/UpdateUser";
import styled from "styled-components";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { EditOutlined, ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

const UsersWrapper = styled.div`
  tr.deactivated {
    background-color: #f5f0be99;
  }

  div.column-active {
    color: green;
  }
  div.column-inactive {
    color: red;
  }
`;

const { confirm } = Modal;
const { Search } = Input;

const actionMenu = ({ record, updateUser }) => (
  <Menu>
    <Menu.Item key="updateUser">
      <Link to={`/users/${record.id}/update`}>
        <EditOutlined />
        &nbsp; Update
      </Link>
    </Menu.Item>
    <Menu.Item key="deactivate user">
      <div
        onClick={() => {
          confirm({
            okText: "Yes",
            okType: "danger",
            cancelText: "No",
            title: `Do you really want to ${
              record.is_active ? "deactivate" : "activate"
            } this user?`,
            content:
              "All the stuff belonging to this plant will be removed forever!",
            onOk() {
              return updateUser({
                variables: {
                  where: {
                    id: record.id,
                  },
                  data: {
                    is_active: !record.is_active,
                  },
                },
              });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; &nbsp; {record.is_active ? "Deactivate" : "Activate"}
      </div>
    </Menu.Item>

    {/* <Menu.Item key="updateAirline">
      <Link to={`/Users/${record.plant_name}/manage_plant_parameters`}>
        <EditOutlined />
        &nbsp; Manage Plant Parameters
      </Link>
    </Menu.Item> */}
    {/* <Menu.Item key="deletePlant">
      <div
        onClick={() => {
          confirm({
            okText: "Yes",
            okType: "danger",
            cancelText: "No",
            title: "Do you really want to delete this plant?",
            content:
              "All the stuff belonging to this plant will be removed forever!",
            onOk() {
              // return deletePlant({
              //   variables: {
              //     plant_name: record.plant_name,
              //   },
              // });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; Delete
      </div>
    </Menu.Item> */}
  </Menu>
);

const columns = (updateUser) => [
  {
    title: "UserID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Full Name",
    key: "is_active",
    render: (text, record) => (
      <>
        {record.first_name} {record.last_name}
      </>
    ),
  },
  {
    title: "User Role",
    dataIndex: "role",
    key: "role",
    align: "center",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    align: "center",
  },
  {
    title: "User Active",
    dataIndex: "is_active",
    key: "os_active",
    align: "center",
    render: (is_active) => {
      if (is_active) {
        return <div className="column-active"> Active </div>;
      } else {
        return <div className="column-inactive"> Inactive </div>;
      }
    },
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, updateUser })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Setting
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const Users = ({ match }) => {
  // const [searchValue, setSearchValue] = useState("");

  const [
    updateUser,
    // { loading, error },
  ] = useMutation(UPDATE_USER_MUTATION, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "User Updated Succesfully" });
    },
    onError: () => {
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: USERS_QUERY }],
  });

  const { loading, error, data, refetch } = useQuery(USERS_QUERY);

  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  const users = data.users;

  console.log(users);

  return (
    <UsersWrapper>
      <TableActions>
        <div className="search-box-wrapper">
          {/* <Search
            value={searchValue}
            className="search-box"
            placeholder="Search a plant"
            onChange={(event) => {
              const { value } = event.target;
              setSearchValue(value);
            }}
          /> */}
        </div>

        <Link to="/users/add">
          <Button
            type="primary"
            // icon="plus"
          >
            Add User
          </Button>
        </Link>
      </TableActions>

      <Table
        rowClassName={(record) => {
          if (!record.is_active) {
            return "deactivated";
          }
        }}
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns(updateUser)}
        dataSource={users}
      />
    </UsersWrapper>
  );
};

export default withRouter(Users);
