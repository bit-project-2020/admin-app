import React from "react";
import { LOGIN_MUTATION } from "../mutations/Login";
import { useMutation } from "@apollo/react-hooks";

import showNotifications from "../lib/showNotifications";
import { CURRENT_USER_QUERY } from "../queries/currentUser";

import { Input, Button, Card, Form as AntForm } from "antd";
import { MailOutlined, KeyOutlined } from "@ant-design/icons";

import { Formik } from "formik";
import * as Yup from "yup";
import ChandimaSweetsLogo from "../assets/images/logo.svg";

const FormItem = AntForm.Item;

const LoginSchema = Yup.object().shape({
  email: Yup.string().email().required("Please enter your email."),
  password: Yup.string().required("Please enter your password."),
});

const Login = () => {
  const [logUserIn] = useMutation(LOGIN_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Login succesful!" });
    },
    onError: (error) => {
      let message = error.message.split(",")[0];
      message = message.split("GraphQL error:")[1];

      showNotifications({
        type: "error",
        message: `Login Failed! ${message}`,
      });
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  return (
    <div>
      <Card
        className="session-form-card"
        title={
          <div className="logo">
            <img
              id="company-logo"
              src={ChandimaSweetsLogo}
              alt="chandima sweets logo"
            />
            <br />
            🙋‍ Welcome! Please Login
          </div>
        }
        actions={[
          <a className="login-form-forgot" href="/reset-password">
            Forgot password
          </a>,
        ]}
      >
        <Formik
          initialValues={{ email: "", password: "" }}
          validationSchema={LoginSchema} // attach the validation schema with form
          onSubmit={async (values, { setSubmitting }) => {
            // log the user in
            await logUserIn({
              variables: {
                ...values,
              },
            });
            setSubmitting(false);
          }}
        >
          {({
            handleSubmit,
            isSubmitting,
            handleBlur,
            handleChange,
            errors,
            touched,
            values,
          }) => (
            <form onSubmit={handleSubmit}>
              <FormItem
                validateStatus={
                  errors.email && touched["email"] ? "error" : "validating"
                }
                help={errors.email}
              >
                <Input
                  name="email"
                  prefix={<MailOutlined />}
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </FormItem>
              <FormItem
                validateStatus={errors.password ? "error" : "validating"}
                help={errors.password}
              >
                <Input
                  name="password"
                  prefix={<KeyOutlined />}
                  type="password"
                  placeholder="Password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </FormItem>

              <FormItem>
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                >
                  Login
                </Button>
              </FormItem>
            </form>
          )}
        </Formik>
      </Card>
    </div>
  );
};

export default Login;
