import React, { useState, useEffect } from "react";
import Chart from "react-google-charts";
import { DatePicker, Space } from "antd";
import { useLazyQuery, useQuery } from "@apollo/react-hooks";
import { NCR_REPORT_QUERY } from "../../queries/reports/ncrReport";
import { PLANTS_QUERY } from "../../queries/plants";

const DefectSeverity = () => {
  const [week, setWeek] = useState(null);
  const [reportDiviation, setReportDiviation] = useState([]);

  const [fetchData, { loading, error, data }] = useLazyQuery(NCR_REPORT_QUERY);
  const plants = useQuery(PLANTS_QUERY);

  useEffect(() => {
    if (week) {
      fetchData({
        variables: {
          where: {
            start_time: week.startOf("week").format("YYYY-MM-DD"),
            end_time: week
              .endOf("week")
              .clone()
              .add(1, "d")
              .format("YYYY-MM-DD"),
          },
        },
      });
    }
  }, [week, fetchData]);

  useEffect(() => {
    if (data && data.ncr_report) {
      let columns = {};
      plants.data.plants.map((plant) => {
        columns[plant.plant_name] = 0;
        return plant;
      });

      let ncr_data = [];
      ncr_data.push(["Day", ...Object.keys(columns)]);

      const day = week.startOf("week").clone();
      for (let i = 0; i < 7; i++) {
        ncr_data.push([day.format("YYYY-MM-DD"), ...Object.values(columns)]);
        day.add(1, "d");
      }

      data.ncr_report.map((value, index) => {
        const row = columns;
        // row["Day"] = value.day
        value.data.map((f) => {
          row[f.plant] = f.count;
          return f;
        });

        ncr_data = ncr_data.map((d) => {
          if (d[0] === value.day) {
            return [value.day, ...Object.values(row)];
          } else {
            return d;
          }
        });
        return value;
      });
      setReportDiviation(ncr_data);
    }
    // eslint-disable-next-line
  }, [week, data]);

  let chart = <div>Please Select the date range</div>;

  if (loading) {
    chart = <div>Loading!</div>;
  }

  if (error) {
    chart = <div>Error!</div>;
  }

  if (data && reportDiviation) {
    chart = (
      <Chart
        width={"700px"}
        height={"400px"}
        chartType="Bar"
        loader={<div>Loading Chart</div>}
        data={reportDiviation}
        options={{
          title: "Defect Sevierity",
        }}
        rootProps={{ "data-testid": "1" }}
      />
    );
  }

  return (
    <div>
      select week <br />
      <Space direction="horizontal">
        <DatePicker
          picker="week"
          value={week}
          allowClear={false}
          onChange={(value) => {
            setWeek(value);
            // console.log(22222222, value);
          }}
        />
        {/* <Select defaultValue="Biscuit" style={{ width: 120 }}>
          <Option value="jack">Biscuit</Option>
          <Option value="lucy">Lucy</Option>
          <Option value="disabled" disabled>
            Disabled
          </Option>
          <Option value="Yiminghe">yiminghe</Option>
        </Select> */}
      </Space>
      <br />
      <br />
      <br />
      {chart}
    </div>
  );
};

export default DefectSeverity;
