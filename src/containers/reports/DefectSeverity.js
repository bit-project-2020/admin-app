import React, { useState, useEffect } from "react";
import Chart from "react-google-charts";
import { DatePicker, Space, Select } from "antd";
import { useLazyQuery, useQuery } from "@apollo/react-hooks";
import { REPORT_DEVIATION_QUERY } from "../../queries/reports/defectSeverity";
import { PLANTS_QUERY } from "../../queries/plants";

const { Option } = Select;
const DefectSeverity = () => {
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [plant, setPlant] = useState("");
  const [reportDiviation, setReportDiviation] = useState([]);

  const [fetchData, { loading, error, data }] = useLazyQuery(REPORT_DEVIATION_QUERY);
  const plants = useQuery(PLANTS_QUERY);

  useEffect(() => {
    if (startTime && endTime) {
      fetchData({
        variables: {
          where: {
            plant: plant,
            start_time: startTime.format("YYYY-MM-DD"),
            end_time: endTime.format("YYYY-MM-DD")
          }
        }
      })
    }
  }, [plant, startTime, endTime, fetchData]);

  useEffect(() => {
    if (data && data.report_deviation) {
      data.report_deviation = data.report_deviation.map((value) => {
        return [value.parameter, value.noOfIncidents]
      })
      setReportDiviation(data.report_deviation)
    }
  }, [data]);

  let chart = (
    <div>Please Select the date range</div>
  )

  if (loading) {
    chart = (
      <div>Loading!</div>
    )
  }

  if (error) {
    chart = (
      <div>Error!</div>
    )
  }


  if (!loading && data) {
    chart = (
      <Chart
        width={"700px"}
        height={"400px"}
        chartType="PieChart"
        loader={<div>Loading Chart</div>}
        data={[
          ["Parameter", "Defect Count"],
          ...reportDiviation
        ]}
        options={{
          title: "Defect Sevierity",
        }}
        rootProps={{ "data-testid": "1" }}
      />
    )
  }


  return (
    <div>
      select dates <br />
      <Space direction="horizontal" style={{ marginBottom: "15px" }}>
        <DatePicker onChange={(d) => { setStartTime(d) }} placeholder="start date" />
        <DatePicker onChange={(d) => { setEndTime(d) }} placeholder="end date" />
        <Select value={plant} style={{ width: 120 }} onChange={(value) => { setPlant(value) }}>
          {!plant.loading && plants.data && plants.data.plants.map((plant) => {
            return <Option value={plant.plant_name} key={plant.plant_name}>{plant.plant_name}</Option>
          })}
        </Select>
      </Space>
      {chart}
    </div>
  );

};

export default DefectSeverity;
