import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Table, Button, Alert, Input, Menu, Modal } from "antd";
import { PARAMETERS_QUERY } from "../queries/Parameters";
import { DELETE_SPEC_PARAMETER_MUTATION } from "../mutations/DeleteSpecParameter";
import { UPDATE_PARAMETER_MUTATION } from "../mutations/UpdateParameter";

import { Link } from "react-router-dom";

import showNotifications from "../lib/showNotifications";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { EditOutlined, ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import styled from "styled-components";

const { confirm } = Modal;
const { Search } = Input;

// todo. If any product is storing that parameter in the system prevent it from deleting.

const ParametersWrapper = styled.div`
  tr.deactivated {
    background-color: #f5f0be99;
  }

  div.column-active {
    color: green;
  }
  div.column-inactive {
    color: red;
  }
`;

const actionMenu = ({ record, deleteSpecParameter, updateParameter }) => (
  <Menu>
    <Menu.Item key="updateParameters">
      {/* toggle parameter */}
      <div
        onClick={() => {
          updateParameter({
            variables: {
              where: {
                name: record.name,
              },
              data: {
                is_active: !record.is_active,
              },
            },
          });
        }}
      >
        <EditOutlined />
        &nbsp;
        {record.is_active ? " Deactivate" : " Activate"}
      </div>
    </Menu.Item>

    <Menu.Item key="deletePlant">
      <div
        onClick={() => {
          confirm({
            okText: "Yes",
            okType: "danger",
            cancelText: "No",
            title: "Do you really want to delete this Parameter?",
            content:
              "All the data gatherd under this parameter will be removed permanently!",
            onOk() {
              return deleteSpecParameter({
                variables: {
                  where: {
                    name: record.name,
                  },
                },
              });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; Delete
      </div>
    </Menu.Item>
  </Menu>
);

const columns = (deleteSpecParameter, updateParameter) => [
  {
    title: "Parameter Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Parameter Type",
    dataIndex: "parameter_type",
    key: "parameter_type",
  },
  {
    title: "Parameter MODE",
    dataIndex: "type",
    key: "type",
  },
  {
    title: "Is Active",
    dataIndex: "is_active",
    key: "is_active",
    render(value, record) {
      if (value) {
        return <div className="column-active"> Active </div>;
      } else {
        return <div className="column-inactive"> Inactive </div>;
      }
    },
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, deleteSpecParameter, updateParameter })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Settings
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const Parameters = ({ history }) => {
  const [searchValue, setSearchValue] = useState("");

  const [deleteSpecParameter] = useMutation(DELETE_SPEC_PARAMETER_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Parameter successfully added!" });
      // history.push("/parameters");
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
      showNotifications({
        type: "error",
        message: `${error.message}`,
      });
    },
    refetchQueries: [{ query: PARAMETERS_QUERY }],
  });
  const [updateParameter] = useMutation(UPDATE_PARAMETER_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Parameter successfully deactivated!" });
      // history.push("/parameters");
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
      showNotifications({
        type: "error",
        message: `${error.message}`,
      });
    },
    refetchQueries: [{ query: PARAMETERS_QUERY }],
  });

  const { loading, error, data } = useQuery(PARAMETERS_QUERY);

  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  if (data) {
    let parameters = data.specParameters;

    parameters = parameters.filter((parameters) =>
      parameters.name.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <ParametersWrapper>
        <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search a parameter"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>

          <Link to="/parameters/add">
            <Button
              type="primary"
              // icon="plus"
            >
              Add a Parameter
            </Button>
          </Link>
        </TableActions>

        <Table
          rowClassName={(record) => {
            if (!record.is_active) {
              return "deactivated";
            }
          }}
          loading={loading}
          bordered
          rowKey={(record) => record.id}
          columns={columns(deleteSpecParameter, updateParameter)}
          dataSource={parameters}
        />
      </ParametersWrapper>
    );
  }
};

export default Parameters;
