import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/react-hooks";
import {
  Table,
  Button,
  Alert,
  Input,
  Modal,
  Space,
  Form,
  DatePicker,
} from "antd";
import { withRouter } from "react-router-dom";
import { LOGS_QUERY } from "../queries/qaologs";
import moment from "moment";
import QAODetail from "../containers/QAODetail";

import { TableActions } from "../components/shared/styledComponents";

import showNotifications from "../lib/showNotifications";

const { Search } = Input;

const columns = (setInitialLog) => [
  {
    title: "Created At",
    dataIndex: "created_at",
    key: "created_at",
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
  },
  {
    title: "User",
    dataIndex: "first_name",
    key: "first_name",
    render: (first_name) => <span>{first_name} </span>,
  },
  {
    title: "Plant",
    dataIndex: "plant",
    key: "plant",
  },
  {
    title: "Role",
    dataIndex: "role",
    key: "role",
  },
  {
    title: "Shift",
    dataIndex: "shift",
    key: "shift",
  },
  {
    title: "Actions",
    dataIndex: "actions",
    align: "center",
    render(txt, log, index) {
      return (
        <Button
          onClick={() => {
            setInitialLog(log);
          }}
        >
          view in detail
        </Button>
      );
    },
  },
];

const QAOLogs = () => {
  const today = new Date();

  let sevenDaysBefore = new Date();
  sevenDaysBefore.setDate(sevenDaysBefore.getDate() - 7);

  const [searchValue, setSearchValue] = useState("");
  const [startDate, setStartDate] = useState(moment(sevenDaysBefore));
  const [endDate, setEndDate] = useState(moment(today));
  const [initialLog, setInitialLog] = useState(null);

  const { loading, error, data, refetch } = useQuery(LOGS_QUERY, {
    variables: {
      start: startDate,
      end: endDate,
    },
  });

  useEffect(() => {
    refetch();
  });

  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  if (data) {
    // manage data here

    let logs = data.logs;
    // filtered data
    logs = logs.filter((log) =>
      log.plant.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <div>
        <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search by plant name"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>
        </TableActions>
        <Space direction={"horizontal"}>
          <Form.Item label="Start Date">
            <DatePicker
              allowClear={false}
              onChange={(date, dateString) => {
                if (endDate.diff(date, "days") < 0) {
                  showNotifications({
                    type: "error",
                    message: "Please select a date before the end date.",
                  });
                } else {
                  setStartDate(date);
                }
              }}
              value={startDate}
            />
          </Form.Item>

          {/* for product */}
          <Form.Item label="End Date">
            <DatePicker
              allowClear={false}
              onChange={(date, dateString) => {
                if (startDate.diff(date, "days") > 0) {
                  showNotifications({
                    type: "error",
                    message: "Please select a date before the end date.",
                  });
                } else {
                  setEndDate(date);
                }
              }}
              value={endDate}
            />
          </Form.Item>
        </Space>
        <Table
          loading={loading}
          bordered
          rowKey={(log) => log.id}
          columns={columns(setInitialLog)}
          dataSource={logs}
        />
        <Modal
          title="QAO Details"
          visible={initialLog !== null}
          onCancel={() => {
            setInitialLog(null);
          }}
          onOk={() => {
            console.log("On OK Pressed");
          }}
          footer={null}
        >
          <QAODetail log={initialLog} />
        </Modal>
      </div>
    );
  }
};

export default withRouter(QAOLogs);
