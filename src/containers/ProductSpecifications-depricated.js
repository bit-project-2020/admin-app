// Attention. Looks like this file is no longer in use. if this is not needed just remove it.

import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Table, Button, Alert, Input, Menu, Modal } from "antd";
import { Link } from "react-router-dom";
import debounce from "lodash/debounce";

import {
  TableActions,
  ActionDropdown,
} from "../components/shared/styledComponents";
import { EditOutlined, ToolOutlined, DeleteOutlined } from "@ant-design/icons";

import showNotifications from "../lib/showNotifications";

const { confirm } = Modal;
const { Search } = Input;

export const PRODUCTS_QUERY = gql`
  query productsQuery {
    products {
      id
      name
      size
      unit
      is_active
    }
  }
`;

const DELETE_PRODUCT_MUTATION = gql`
  mutation deleteProduct($where: ProductWhereUniqueInput!) {
    deleteProduct(where: $where) {
      id
      plant
      name
      size
      unit
      is_active
    }
  }
`;

const actionMenu = ({ record, deleteProduct }) => (
  <Menu>
    <Menu.Item key="updateProduct">
      <Link to={`/plants/${record.plant_name}/update`}>
        <EditOutlined />
        &nbsp; Update
      </Link>
    </Menu.Item>

    <Menu.Item key="updateProduct">
      <Link to={`/products/${record.id}/manage_specifications`}>
        <EditOutlined />
        &nbsp; Manage Specifications
      </Link>
    </Menu.Item>

    <Menu.Item key="deletePlant">
      <div
        onClick={() => {
          confirm({
            okText: "Yes",
            okType: "danger",
            cancelText: "No",
            title: "Do you really want to delete this product?",
            content:
              "All the products that associated with this product will be removed!",
            onOk() {
              return deleteProduct({
                variables: {
                  where: {
                    id: record.id,
                  },
                },
              });
            },
            onCancel() {},
          });
        }}
        role="presentation"
      >
        <DeleteOutlined />
        &nbsp; Delete
      </div>
    </Menu.Item>
  </Menu>
);

const columns = (deleteProduct) => [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  // temporally removing size and unit
  // {
  //   title: 'Size',
  //   dataIndex: 'size',
  //   key: 'name'
  // },
  // {
  //   title: 'Unit',
  //   dataIndex: 'unit',
  //   key: 'name'
  // },
  {
    title: "isActive",
    dataIndex: "is_active",
    key: "is_active",
    render: (is_active) => <span>{is_active ? "Yes" : "No"} </span>,
  },
  {
    title: "Actions",
    dataIndex: "actions",
    key: "actions",
    align: "center",
    render(text, record) {
      return (
        <ActionDropdown
          overlay={actionMenu({ record, deleteProduct })}
          trigger={["click"]}
        >
          <h1>
            <ToolOutlined />
            &nbsp; Setting
          </h1>
        </ActionDropdown>
      );
    },
  },
];

const filterPlants = debounce(({ value, refetch }) => {
  refetch({ where: { name_contains: value } });
}, 500);

const Products = () => {
  const [searchValue, setSearchValue] = useState("");

  const [
    deleteProduct,
    // { loading: deleteAirlineLoading, error: deleteAirlineError },
  ] = useMutation(DELETE_PRODUCT_MUTATION, {
    onCompleted: () => {
      console.log(222, "on completed called");
      showNotifications({ message: "Flight successfully added!" });
    },
    onError: () => {
      console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: PRODUCTS_QUERY }],
  });

  const { loading, error, data, refetch } = useQuery(PRODUCTS_QUERY);

  if (error) {
    return (
      <Alert
        message="Error"
        description="Something went wrong! Please try again."
        type="error"
        showIcon
      />
    );
  }
  if (loading) {
    return <h1> Loading....! </h1>;
  }

  console.log(data);

  return (
    <div>
      <TableActions>
        <div className="search-box-wrapper">
          <Search
            value={searchValue}
            className="search-box"
            placeholder="Search a product"
            onChange={(event) => {
              const { value } = event.target;
              setSearchValue(value);
              filterPlants({ value, refetch });
            }}
          />
        </div>

        <Link to="/products/add">
          <Button
            type="primary"
            // icon="plus"
          >
            Add Product
          </Button>
        </Link>
      </TableActions>

      <Table
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns(deleteProduct)}
        dataSource={data.products}
      />
    </div>
  );
};

export default Products;
