import { gql } from "apollo-boost";

const BACKUPS_QUERY = gql`
  query backups_query {
    backups {
      name
      __typename
    }
  }
`;

export { BACKUPS_QUERY };
