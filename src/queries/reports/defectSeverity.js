import { gql } from "apollo-boost";

const REPORT_DEVIATION_QUERY = gql`
  query report_deviation_query($where: DeviationReportWhereInput!) {
    report_deviation(where: $where) {
        parameter
        noOfIncidents
    }
  }
`;

export { REPORT_DEVIATION_QUERY };
