import { gql } from "apollo-boost";

const NCR_REPORT_QUERY = gql`
    query ncr_report_query($where: NCRCountWhereInput!) {
        ncr_report(where: $where) {
            day
            data{
                plant
                count
            }
        }
    }
`;

export { NCR_REPORT_QUERY };
