import { gql } from "apollo-boost";

const PLANT_PARAMETERS_QUERY = gql`
  query PlantParametersQuery($where: PlantParametersWhereInput) {
    plantParameters(where: $where) {
      id
      plant
      parameter
      is_active
      unit
      parameter_type
      type
    }
  }
`;

export { PLANT_PARAMETERS_QUERY };
