import { gql } from "apollo-boost";

const PLANTS_QUERY = gql`
  query PlantQuery($where: PlantWhereUniqueInput) {
    plants(where: $where) {
      plant_name
      is_active
      image_url
    }
  }
`;

export { PLANTS_QUERY };
