import { gql } from "apollo-boost";

const INSPECTION_DATA_QUERY = gql`
  query inspection_data_query($where: DataWhereInput!) {
    inspectionData(where: $where) {
      id
      date
      shift
      comment
      is_deviated
      extra_fields {
        parameter
        type
        text_value
        numeric_value
        boolean_value
      }
    }
  }
`;

export { INSPECTION_DATA_QUERY };
