import { gql } from "apollo-boost";

const PRODUCT_QUERY = gql`
  query product($where: ProductWhereUniqueInput!) {
    product(where: $where) {
      id
      plant
      name
      image_url
      is_active
    }
  }
`;

export { PRODUCT_QUERY };
