import { gql } from "apollo-boost";

export const USERS_QUERY = gql`
  query usersQuery {
    users {
      id
      last_name
      first_name
      role
      department
      emp_no
      user_name
      is_active
      email
      profile_photo
    }
  }
`;
