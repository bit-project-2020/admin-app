import { gql } from "apollo-boost";

const SIZES_QUERY = gql`
  query SizesQuery($where: SizeWhereInput!) {
    sizes(where: $where) {
      id
      product
      size_name
      is_active
    }
  }
`;

export { SIZES_QUERY };
