import { gql } from "apollo-boost";

const PRODUCTS_QUERY = gql`
  query productsQuery($where: ProductWhereInput) {
    products(where: $where) {
      id
      name
      plant
      is_active
      image_url
    }
  }
`;

export { PRODUCTS_QUERY };
