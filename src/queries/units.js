import { gql } from "apollo-boost";

const UNITS_QUERY = gql`
  query unitsQuery {
    units {
      unit
      description
    }
  }
`;

export { UNITS_QUERY };
