import { gql } from "apollo-boost";

const SPECIFICATIONS_QUERY = gql`
  query specificatoins_query($product_id: Int!) {
    specifications(where: { id: $product_id }) {
      id
      product
      plant_parameter
      size
      is_default
      is_active
      value
    }
  }
`;

export { SPECIFICATIONS_QUERY };
