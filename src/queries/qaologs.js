import { gql } from "apollo-boost";

const LOGS_QUERY = gql`
  query getLogsQuery($start: Date, $end: Date) {
    logs(where: {created_at_lt: $end, created_at_gt: $start}){
    id
    plant
    created_by
    created_at
    title
    log
    first_name
    last_name
    profile_photo
    role
    shift
  }
}
`;

export { LOGS_QUERY };