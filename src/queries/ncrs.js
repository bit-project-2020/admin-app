import { gql } from "apollo-boost";

const NCRS_QUERY = gql`
  query ncr_query($where: NCRWhereInput) {
    ncrs(where: $where) {
      id
      created_by
      created_at
      reason
      type
      plant
      created_at
      size_name
      is_active
      record_id
      product_name
    }
  }
`;

export { NCRS_QUERY };
