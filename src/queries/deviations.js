import { gql } from "apollo-boost";

const DEVIATIONS_QUERY = gql`
  query deviations_query($product_id: Int!) {
    deviations(where: { product: $product_id }) {
      id
      product
      plant_parameter
      size
      is_active
    }
  }
`;

export { DEVIATIONS_QUERY };
