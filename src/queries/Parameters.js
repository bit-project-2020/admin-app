import { gql } from "apollo-boost";

const PARAMETERS_QUERY = gql`
  query specParameters {
    specParameters {
      name
      parameter_type
      type
      is_active
    }
  }
`;

export { PARAMETERS_QUERY };
